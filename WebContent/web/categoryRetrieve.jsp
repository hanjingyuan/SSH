<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>OnlineData</title>
  <meta name="description" content="">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/web/assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/web/assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/admin.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<header class="am-topbar admin-header">
  <div class="am-topbar-brand">
    <strong>OnlineData</strong> <small>在线制图管理系统</small>
  </div>

  <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>
	<!-- 上方标签 -->
	<%@ include file="menuTop.jsp" %>
	<!-- 上方标签 -->
</header>

<div class="am-cf admin-main">
  <!-- sidebar start -->
  <div class="admin-sidebar">
	
	<!-- 左侧导航开始 -->
	<%@ include file="/web/menuLeft.jsp" %>
	<!-- 左侧导航结束-->
    
    <!-- 左下侧广播 -->
    <%@ include file="/web/menuAd.jsp" %>
    <!-- 左下侧广播 -->
  
  <!-- sidebar end -->
<!-- content start -->
  <div class="admin-content">
    <div class="am-cf am-padding">
      <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">表格</strong> / <small>Table</small></div>
    </div>
    <div class="am-g">
      <div class="am-u-sm-12">
        <form class="am-form">
          <table class="am-table am-table-striped am-table-hover table-main">
            <thead>
              <tr>
                <th class="table-id">ID</th>
                <th class="table-title">名称</th>
                <th class="table-type">数据一</th>
                <th class="table-author">数据二</th>
                <th class="table-date">数据三</th>
                <th class="table-set">编辑</th>
              </tr>
          </thead>
          <s:iterator var="c" value="clist" status="status">
          <tbody>
            <tr>
              <td><s:property value="#c.caid" /></td>
              <td><s:property value="#c.name" /></td>
              <td><s:property value="#c.data1" /></td>
              <td><s:property value="#c.data2" /></td>
              <td><s:property value="#c.data3" /></td>
              <td>
                <div class="am-btn-toolbar">
                  <div class="am-btn-group am-btn-group-xs">
                    <a href="${pageContext.request.contextPath}/category_edit?caid=<s:property value="#c.caid"/>">编辑</a>      
                    <a href="${pageContext.request.contextPath}/category_delete?caid=<s:property value="#c.caid"/>">删除</a>
                  </div>
                </div>
              </td>
            </tr>          
          </tbody>
          </s:iterator>
        </table>
          <hr />
        </form>
      </div>
    </div>
     <strong>如果点击编辑或者删除没反应，那么多点几次，这个bug暂时还没找到解决方案。</strong>
  </div>
  <!-- content end -->
		
<footer>
  <hr>
  <p class="am-padding-left">© Powered by 汉景源. <a href="http://blog.csdn.net/lucahan" target="_blank">我的个人博客</a></p>
</footer>

<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/web/assets/js/jquery1.11.1.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/modernizr.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/rem.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/respond.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.legacy.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="${pageContext.request.contextPath}/web/assets/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.min.js"></script>
<!--<![endif]-->
<script src="${pageContext.request.contextPath}/web/assets/js/app.js"></script>
</body>
</html>