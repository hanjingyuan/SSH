<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>模板</title>
    <!--示例网址-->
    <!--中文介绍-->
    <!---->
</head>
<body>
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<div id="main" style="height:800px"></div>
<!-- ECharts单文件引入 -->
<script src="http://echarts.baidu.com/build/dist/echarts.js"></script>
<script type="text/javascript">
    // 路径配置
    require.config({
        paths: {
            echarts: 'http://echarts.baidu.com/build/dist'
        }
    });

    // 使用
    require(
            [
                'echarts',
                'echarts/chart/bar', // 使用柱状图就加载bar模块，按需加载
                'echarts/chart/line',
                'echarts/chart/pie',
                'echarts/chart/map',
                'echarts/chart/gauge',
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main'));

                var option = {
                    tooltip : { //提示框，鼠标悬浮交互时的信息提示。
                        trigger: 'item'
                        //item与axis的区别
                        //item只会显示一个。
                        //而axis会显示所有的数据
                    },
                    toolbox: { //工具箱，用来辅助使用，增强功能
                        show : true,
                        feature : {
                            mark : {show: true},
                            dataView : {show: true, readOnly: false},
                            magicType: {show: true, type: ['line', 'bar']},
                            restore : {show: true},
                            saveAsImage : {show: true}
                        }
                    },
                    //calculable : true, 拖拽特性，我觉得没用
                    legend: { //图例
                        data:['城市绿地面积','绿化覆盖率']
                    },
                    xAxis : [ //X轴
                        {
                            type : 'category',
                            data : ['北京', '天津', '河北', '山西', '内蒙古', '辽宁', '吉林', 
		                            '黑龙江', '上海', '江苏', '浙江', '安徽', '福建', '江西', '山东', 
		                            '河南', '湖北', '湖南', '广东', '广西', '海南', '重庆', 
                            		'四川', '贵州', '云南', '西藏', '陕西', '甘肃', '青海', '宁夏', '新疆']
                        }
                    ],
                    yAxis : [ //Y轴
                        {
                            type : 'value',
                            name : '面积 ',
                            axisLabel : {
                                formatter: '{value} 公顷 '
                            }
                        },
                        {
                            type : 'value',
                            name : '覆盖率',
                            axisLabel : {
                                formatter: '{value} %'
                            }
                        }
                    ],
                    series : [ //这个是核心，驱动下面的数组生成图标
                        {
                            name:'城市绿地面积',
                            type:'bar',
                            data:[62672, 19221, 68958, 31061, 38143, 92751, 37895, 69581, 120148, 227584, 79459, 71463, 47904, 42288, 156243, 66790, 57883, 46028, 420370, 60225, 49029, 37695, 72259, 28675, 28126, 2090, 26063, 15275, 3387, 17387, 37686]
                        },
                        {
                            name:'绿化覆盖率',
                            type:'line',
                            yAxisIndex: 1,
                            data:[38.6, 32.1, 42.7, 38, 33.4, 39.3, 34.1, 34.9, 38.2, 42.1, 38.3, 37.5, 41, 46.6, 41.5, 36.6, 37.7, 36.6, 41.3, 35, 42.6, 40.6, 37.9, 29.6, 37.3, 25.4, 38.3, 27.1, 29.4, 38.8, 36.4]
                        },

                    ]
                };

                // 为echarts对象加载数据
                myChart.setOption(option);
            }
    );
</script>
</body>
</html>