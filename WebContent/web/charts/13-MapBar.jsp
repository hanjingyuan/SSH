<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>模板</title>
    <!--示例网址-->
    <!--中文介绍-->
    <!---->
    <style type="text/css">
       body{margin: 0;padding: 0;}
       #main{width: 50%;height: 800px;margin: 0 auto;float: left;}
       #main2{width: 50%;height: 800px;margin: 0 auto;float: right;}
    </style>
</head>
<body>
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<div id="main" style="height:800px" ></div>
<div id="main2" style="height:800px"></div>
<!-- ECharts单文件引入 -->
<script src="http://echarts.baidu.com/build/dist/echarts.js"></script>
<script type="text/javascript">
    // 路径配置
    require.config({
        paths: {
            echarts: 'http://echarts.baidu.com/build/dist'
        }
    });

    // 使用
    require(
            [
                'echarts',
                'echarts/chart/bar', // 使用柱状图就加载bar模块，按需加载
                'echarts/chart/line',
                'echarts/chart/pie',
                'echarts/chart/map',
                'echarts/chart/gauge',
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main'));
                var myChart2 = ec.init(document.getElementById('main2'));

                var  option = {
                    title : {
                        text: '2010年中国各省人口数量（单位：万）',
                        //subtext: '数据来自网络'
                    },
                    tooltip : {
                        trigger: 'axis'
                    },
                    legend: {
                        data:['2010年']
                    },
                    toolbox: {
                        show : true,
                        feature : {
                            mark : {show: true},
                            dataView : {show: true, readOnly: false},
                            magicType: {show: true, type: ['line', 'bar']},
                            restore : {show: true},
                            saveAsImage : {show: true}
                        }
                    },
                    calculable : true,
                    xAxis : [
                        {
                            type : 'value',
                            boundaryGap : [0, 0.01]
                        }
                    ],
                    yAxis : [
                        {
                            type : 'category',
                            data : ['北京', '天津', '河北', '山西', '内蒙古', '辽宁', '吉林', '黑龙江', '上海', '江苏', '浙江', '安徽', '福建', '江西', '山东', '河南', '湖北', '湖南', '广东', '广西', '海南', '重庆', '四川', '贵州', '云南', '西藏', '陕西', '甘肃', '青海', '宁夏', '新疆']
                        }
                    ],
                    series : [
                        {
                            name:'2010年',
                            type:'bar',
                            data:[1962, 1299, 7194, 3574, 2472, 4375, 2747, 3833, 2303, 7869, 5447, 5957, 3693, 4462, 9588, 9405, 5728, 6570, 10441, 4610, 869, 2885, 8045, 3479, 4602, 301, 3735, 2560, 563, 633, 2185]
                        }
                    ]
                };
                
                var option2 = {

                    tooltip : {
                        trigger: 'item',
                        formatter: "{a} <br/>{b}  :{c}万人"
                    },
                    legend: {
                        orient: 'vertical',
                        x:'left',
                        data:['人口总量']
                    },
                    dataRange: {
                        min: 0,
                        max: 11000,
                        x: 'left',
                        y: 'bottom',
                        text:['高','低'],           // 文本，默认为数值文本
                        calculable : true
                    },
                    toolbox: {
                        show: true,
                        orient : 'vertical',
                        x: 'right',
                        y: 'center',
                        feature : {
                            mark : {show: true},
                            dataView : {show: true, readOnly: false},
                            restore : {show: true},
                            saveAsImage : {show: true}
                        }
                    },
                    roamController: {
                        show: true,
                        x: 'right',
                        mapTypeControl: {
                            'china': true
                        }
                    },
                    series : [
                        {
                            name: '人口总量',
                            type: 'map',
                            mapType: 'china',
                            roam: false,
                            itemStyle:{
                                normal:{label:{show:true}},
                                emphasis:{label:{show:true}}
                            },
                            data:[
								    {name: '北京', value: 1962},
								    {name: '天津', value: 1299},
								    {name: '河北', value: 7194},
								    {name: '山西', value: 3574},
								    {name: '内蒙古', value: 2472},
								    {name: '辽宁', value: 4375},
								    {name: '吉林', value: 2747},
								    {name: '黑龙江', value: 3833},
								    {name: '上海', value: 2303},
								    {name: '江苏', value: 7869},
								    {name: '浙江', value: 5447},
								    {name: '安徽', value: 5957},
								    {name: '福建', value: 3693},
								    {name: '江西', value: 4462},
								    {name: '山东', value: 9588},
								    {name: '河南', value: 9405},
								    {name: '湖北', value: 5728},
								    {name: '湖南', value: 6570},
								    {name: '广东', value: 10441},
								    {name: '广西', value: 4610},
								    {name: '海南', value: 869},
								    {name: '重庆', value: 2885},
								    {name: '四川', value: 8045},
								    {name: '贵州', value: 3479},
								    {name: '云南', value: 4602},
								    {name: '西藏', value: 301},
								    {name: '陕西', value: 3735},
								    {name: '甘肃', value: 2560},
								    {name: '青海', value: 563},
								    {name: '宁夏', value: 633},
								    {name: '新疆', value: 2185}
								]
                        }
                                            
                    ]
                };
                
				 myChart.connect(myChart2);
                myChart2.connect(myChart);
                // 为echarts对象加载数据
                myChart.setOption(option);
                myChart2.setOption(option2);
            }
    );
</script>
</body>
</html>