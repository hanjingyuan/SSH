<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <!--示例网址-->
    <!--中文介绍-->
    <!---->
    <style type="text/css">
        body{margin: 0;padding: 0;}
       #main{width: 50%;height: 700px;margin: 0 auto;float: left;}
       #main2{width: 50%;height: 700px;margin: 0 auto;float: right;}
    </style>
</head>
<body>
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<div id="main" style="height:700px" ></div>
<div id="main2" style="height:700px"></div>
<!-- ECharts单文件引入 -->
<script src="http://echarts.baidu.com/build/dist/echarts.js"></script>
<script type="text/javascript">
    // 路径配置
    require.config({
        paths: {
            echarts: 'http://echarts.baidu.com/build/dist'
        }
    });

    // 使用
    require(
            [
                'echarts',
                'echarts/chart/bar', // 使用柱状图就加载bar模块，按需加载
                'echarts/chart/pie'
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main'));
                var myChart2 = ec.init(document.getElementById('main2'));
                var option = {
                    title : {
                        text: '华东七省人口情况',
                        subtext: '2010年',
                        x:'center'
                    },
                    tooltip : {
                        trigger: 'item',
                        formatter: "{a} <br/>{b} : ({c} 万人)({d}%)"
                    },
                    legend: {
                        orient : 'vertical',
                        x : 'left',
                        data:['山东', '江苏', '安徽', '上海', '浙江', '福建', '江西']
                    },
                    calculable : true,
                    series : [
                        {
                            name:'各省人口',
                            type:'pie',
                            radius : '55%',
                            center: ['50%', 225],
                            data:[
								    {name: '山东', value: 9579},
								    {name: '江苏', value: 7866},
								    {name: '安徽', value: 5950},
								    {name: '上海', value: 2302},
								    {name: '浙江', value: 5443},
								    {name: '福建', value: 3689},
								    {name: '江西', value: 4457}
								]
                        }
                    ]
                };

                var  option2 = {
                    tooltip : {
                        trigger: 'axis',
                        axisPointer : {
                            type: 'shadow'
                        }
                    },
                    legend: {
                        data:['男性人口', '女性人口']
                    },
                    toolbox: {
                        show : true,
                        orient : 'vertical',
                        y : 'center',
                        feature : {
                            mark : {show: true},
                            magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                            restore : {show: true},
                            saveAsImage : {show: true}
                        }
                    },
                    calculable : true,
                    xAxis : [
                        {
                            type : 'category',
                            data : ['山东', '江苏', '安徽', '上海', '浙江', '福建', '江西']
                        }
                    ],
                    yAxis : [
                        {
                            type : 'value',
                            splitArea : {show : true}
                        }
                    ],
                    grid: {
                        x2:40
                    },
                    series : [
                        {
                            name:'男性人口',
                            type:'bar',
                            stack: '总量',
                            data:[4845, 3963, 3025, 1185, 2797, 1898, 2308]
                        },
                        {
                            name:'女性人口',
                            type:'bar',
                            stack: '总量',
                            data:[4735, 3903, 2926, 1116, 2646, 1791, 2148]
                        },
                    ]
                };

                myChart2.setOption(option2);
                myChart.connect(myChart2);
                myChart2.connect(myChart);

                setTimeout(function (){
                    window.onresize = function () {
                        myChart.resize();
                        myChart2.resize();
                    }
                },200)

                // 为echarts对象加载数据
                myChart.setOption(option);
            }
    );
</script>
</body>
</html>