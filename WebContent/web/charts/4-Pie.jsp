<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>模板</title>
    <!--示例网址-->
    <!--中文介绍-->
    <!--吉林省三次产业就业人员构成-->
</head>
<body>
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<div id="main" style="height:800px"></div>
<!-- ECharts单文件引入 -->
<script src="http://echarts.baidu.com/build/dist/echarts.js"></script>
<script type="text/javascript">
    // 路径配置
    require.config({
        paths: {
            echarts: 'http://echarts.baidu.com/build/dist'
        }
    });

    // 使用
    require(
            [
                'echarts',
                'echarts/chart/bar', // 使用柱状图就加载bar模块，按需加载
                'echarts/chart/line',
                'echarts/chart/pie',
                'echarts/chart/map',
                'echarts/chart/gauge',
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main'));

                var option = {
                    title : {
                        text: '北京市财政支出',
                        subtext: '2010年',
                        x:'center'
                    },
                    tooltip : {
                        trigger: 'item',
                        formatter: "{a} <br/>{b}  :{c}万元  {d}%"
                    },
                    legend: {
                        orient : 'vertical',
                        x : 'left',
                        data:['一般公共服务', '国防', '公共安全', '教育', '科学技术', '文化体育与传媒', '社会保障和就业', '医疗卫生', '坏境保护', '城乡社区事务', '农林水事务', '交通运输', '资源勘探电力信息等事务', '商业服务业等事务', '金融监管等事务支出', '地震灾后恢复重建支出', '国土资源气象等事务', '住房保障支出', '粮油物资储备管理事务', '国债还本付息支出', '其他支出']
                    },
                    toolbox: {
                        show : true,
                        feature : {
                            mark : {show: true},
                            dataView : {show: true, readOnly: false},
                            magicType : {
                                show: true,
                                type: ['pie', 'funnel'],
                                option: {
                                    funnel: {
                                        x: '25%',
                                        width: '50%',
                                        funnelAlign: 'left',
                                        max: 1548
                                    }
                                }
                            },
                            restore : {show: true},
                            saveAsImage : {show: true}
                        }
                    },
                    calculable : true,
                    series : [
                        {
                            name:'北京市财政支出',
                            type:'pie',
                            radius : '55%',
                            center: ['50%', '60%'],
                            data:[
							    {name: '一般公共服务', value: 8499.74},
							    {name: '国防', value: 157.02},
							    {name: '公共安全', value: 4642.5},
							    {name: '教育', value: 11829.06},
							    {name: '科学技术', value: 1588.88},
							    {name: '文化体育与传媒', value: 1392.57},
							    {name: '社会保障和就业', value: 8680.32},
							    {name: '医疗卫生', value: 4730.62},
							    {name: '坏境保护', value: 2372.5},
							    {name: '城乡社区事务', value: 5977.29},
							    {name: '农林水事务', value: 7741.69},
							    {name: '交通运输', value: 3998.89},
							    {name: '资源勘探电力信息等事务', value: 2996.65},
							    {name: '商业服务业等事务', value: 1273.35},
							    {name: '金融监管等事务支出', value: 148.88},
							    {name: '地震灾后恢复重建支出', value: 1094.64},
							    {name: '国土资源气象等事务', value: 1153.99},
							    {name: '住房保障支出', value: 1990.4},
							    {name: '粮油物资储备管理事务', value: 676.84},
							    {name: '国债还本付息支出', value: 335.36},
							    {name: '其他支出', value: 2602.06}
							]
                        }
                    ]
                };

                // 为echarts对象加载数据
                myChart.setOption(option);
            }
    );
</script>
</body>
</html>