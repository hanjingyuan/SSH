<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>模板</title>
    <!--示例网址-->
    <!--中文介绍-->
    <!---->
</head>
<body>
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<div id="main" style="height:650px"></div>
<!-- ECharts单文件引入 -->
<script src="http://echarts.baidu.com/build/dist/echarts.js"></script>
<script type="text/javascript">
	
	var ex=[
	        {
	            "name": "北京",
	            "value": "1961.0"
	        },
	        {
	            "name": "天津",
	            "value": "1294.0"
	        },
	        {
	            "name": "河北",
	            "value": "7185.0"
	        },
	        {
	            "name": "山西",
	            "value": "3571.0"
	        },
	        {
	            "name": "内蒙古",
	            "value": "2471.0"
	        },
	        {
	            "name": "辽宁",
	            "value": "4375.0"
	        },
	        {
	            "name": "吉林",
	            "value": "2746.0"
	        },
	        {
	            "name": "黑龙江",
	            "value": "3831.0"
	        },
	        {
	            "name": "上海",
	            "value": "2302.0"
	        },
	        {
	            "name": "江苏",
	            "value": "7866.0"
	        },
	        {
	            "name": "浙江",
	            "value": "5443.0"
	        },
	        {
	            "name": "安徽",
	            "value": "5950.0"
	        },
	        {
	            "name": "福建",
	            "value": "3689.0"
	        },
	        {
	            "name": "江西",
	            "value": "4457.0"
	        },
	        {
	            "name": "山东",
	            "value": "9579.0"
	        },
	        {
	            "name": "河南",
	            "value": "9402.0"
	        },
	        {
	            "name": "湖北",
	            "value": "5724.0"
	        },
	        {
	            "name": "湖南",
	            "value": "6568.0"
	        },
	        {
	            "name": "广东",
	            "value": "10430.0"
	        },
	        {
	            "name": "广西",
	            "value": "4603.0"
	        },
	        {
	            "name": "海南",
	            "value": "867.0"
	        },
	        {
	            "name": "重庆",
	            "value": "2885.0"
	        },
	        {
	            "name": "四川",
	            "value": "8042.0"
	        },
	        {
	            "name": "贵州",
	            "value": "3475.0"
	        },
	        {
	            "name": "云南",
	            "value": "4597.0"
	        },
	        {
	            "name": "西藏",
	            "value": "300.0"
	        },
	        {
	            "name": "陕西",
	            "value": "3733.0"
	        },
	        {
	            "name": "甘肃",
	            "value": "2558.0"
	        },
	        {
	            "name": "青海",
	            "value": "563.0"
	        },
	        {
	            "name": "宁夏",
	            "value": "630.0"
	        },
	        {
	            "name": "新疆",
	            "value": "2181.0"
	        },
	        {
	            "name": "香港",
	            "value": "710.0"
	        },
	        {
	            "name": "澳门",
	            "value": "55.0"
	        },
	        {
	            "name": "台湾",
	            "value": "2316.0"
	        }
	    ]
		//配置信息：这六个分别代表了配置信息，从上到下分别为
		//图像名称，单位，图例数组，数据图例书名，值域最大值，值域最小值
		//但是edit页面做5个input就可以了，图例这里可以共用
		var ex_text="2010年中国人口统计图";
		var ex_subtext="单位：万人";
		var ex_legend=["人口数量"];
		var ex_series="人口数量";
		var ex_min="0";
		var ex_max="11000";
	
	
    // 路径配置
    require.config({
        paths: {
            echarts: 'http://echarts.baidu.com/build/dist'
        }
    });

    // 使用
    require(
            [
                'echarts',
                'echarts/chart/bar', // 使用柱状图就加载bar模块，按需加载
                'echarts/chart/line',
                'echarts/chart/pie',
                'echarts/chart/map',
                'echarts/chart/gauge',
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main'));

                var option = {
                    title : {
                    	//1.主标题，2.单位
                        text: ex_text,
                        subtext:ex_subtext,
                        x:'center'
                    },
                    tooltip : {
                        trigger: 'item',                       
                    },
                    
                    legend: {
                        orient: 'vertical',
                        x:'left',
                        //3.图例数组
                        data:ex_legend
                    },
                    dataRange: {
                    	//5.min和max代表值域范围
                        min: ex_min,
                        max: ex_max,
                        x: 'left',
                        y: 'bottom',
                        text:['高','低'],           // 文本，默认为数值文本
                        calculable : true
                    },
                    toolbox: {
                        show: true,
                        orient : 'vertical',
                        x: 'right',
                        y: 'center',
                        feature : {
                            mark : {show: true},
                            dataView : {show: true, readOnly: false},
                            restore : {show: true},
                            saveAsImage : {show: true}
                        }
                    },
                    roamController: {
                        show: true,
                        x: 'right',
                        mapTypeControl: {
                            'china': true
                        }
                    },
                    series : [
                        {
                        	//6.点击数据时的名称，可以和图例通用一下
                            name: ex_series,
                            type: 'map',
                            mapType: 'china',
                            roam: false,
                            itemStyle:{
                                normal:{label:{show:true}},
                                emphasis:{label:{show:true}}
                            },
                            data:ex
                        }
                                            
                    ]
                };

                // 为echarts对象加载数据
                myChart.setOption(option);
            }
    );
</script>
</body>
</html>