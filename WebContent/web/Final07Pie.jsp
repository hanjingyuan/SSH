<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>OnlineData</title>
  <meta name="description" content="">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/web/assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/web/assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/admin.css">
  <script src="${pageContext.request.contextPath}/web/assets/js/jquery.min.js"></script>
</head>
<body onload="my()">
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<header class="am-topbar admin-header">
  <div class="am-topbar-brand">
    <strong>OnlineData</strong> <small>在线制图管理系统</small>
  </div>

  <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>
	<!-- 上方标签 -->
	<%@ include file="menuTop.jsp" %>
	<!-- 上方标签 -->
</header>

<div class="am-cf admin-main">
  <!-- sidebar start -->
  <div class="admin-sidebar">
	
	<!-- 左侧导航开始 -->
	<%@ include file="/web/menuLeft.jsp" %>
	<!-- 左侧导航结束-->
    
    <!-- 左下侧广播 -->
    <%@ include file="/web/menuAd.jsp" %>
    <!-- 左下侧广播 -->
  
  <!-- sidebar end -->
<!-- content start -->
  <div class="admin-content">
	<div id="main" style="height:800px"></div>
	<div id="namin2"><button id="testbutton" value="button"></button></div>
  </div>
  
  <script src="http://echarts.baidu.com/build/dist/echarts.js"></script>
  <script src="http://echarts.baidu.com/build/dist/echarts.js"></script>
	<script type="text/javascript">
		var jsondata="<s:property value="#session.jsondata"/>";
		jsondata=jsondata.replace(/&quot;/g,'"');
		
		var jsonoption="<s:property value="#session.jsonoption"/>";
		jsonoption=jsonoption.replace(/&quot;/g,'"');
		 
	
		 
			//配置信息：这六个分别代表了配置信息，从上到下分别为
			//图像名称，单位，图例数组，数据图例书名，值域最大值，值域最小值
			//但是edit页面做5个input就可以了，图例这里可以共用
			var reg = new RegExp('"',"g");
			var ex_text="默认名称";
			var ex_subtext="默认单位";
			var ex_legend=["图例"];
			var ex_series="图例";
			var ex_min="0";
			var ex_max="5000";
			
		function my(){
			var json=JSON.parse(jsondata);
		    //console.log(json);
		    var json_option=JSON.parse(jsonoption);
		    console.log(json_option);
		    console.log(json);
		    for(var i=0;i<json.length;i++)
		    {		    	
		    	ex_legend[i]=json[i].name;
		    }
		    
		    //这里的问题就是要把双印号都干掉！！！
			ex_text=json_option[0].value;
			ex_subtext="单位："+json_option[1].value;
			
			//这里的逻辑是先把json对象string化，再去掉string化后的双引号
			ex_series=JSON.stringify(json_option[2].value);
			ex_series=ex_series.replace(reg, "")
			
			ex_legend[0]=ex_series;
	
			//console.log(typeof(ex_max));  

			// 路径配置
		    require.config({
		        paths: {
		            echarts: 'http://echarts.baidu.com/build/dist'
		        }
		    });

		    // 使用
		    require(
		            [
		                'echarts',
		                'echarts/chart/bar', // 使用柱状图就加载bar模块，按需加载
		                'echarts/chart/line',
		                'echarts/chart/pie',
		                'echarts/chart/map',
		                'echarts/chart/gauge',
		            ],
		            function (ec) {
		                // 基于准备好的dom，初始化echarts图表
		                var myChart = ec.init(document.getElementById('main'));

		                var option = {
		                    title : {
		                        text: ex_text,
		                        subtext: ex_subtext,
		                        x:'center'
		                    },
		                    tooltip : {
		                        trigger: 'item',
		                        
		                    },
		                    legend: {
		                        orient : 'vertical',
		                        x : 'left',
		                        data:ex_legend
		                    },
		                    toolbox: {
		                        show : true,
		                        feature : {
		                            mark : {show: true},
		                            dataView : {show: true, readOnly: false},
		                            magicType : {
		                                show: true,
		                                type: ['pie', 'funnel'],
		                                option: {
		                                    funnel: {
		                                        x: '25%',
		                                        width: '50%',
		                                        funnelAlign: 'left',
		                                        max: 1548
		                                    }
		                                }
		                            },
		                            restore : {show: true},
		                            saveAsImage : {show: true}
		                        }
		                    },
		                    calculable : true,
		                    series : [
		                        {
		                            name:ex_series,
		                            type:'pie',
		                            radius : '55%',
		                            center: ['50%', '60%'],
		                            data:json,
		                        }
		                    ]
		                };

		                // 为echarts对象加载数据
		                myChart.setOption(option);
		            }
		    );
		}
	</script>
  <!-- content end -->
		
<footer>
  <hr>
  <p class="am-padding-left">© Powered by 汉景源. <a href="http://blog.csdn.net/lucahan" target="_blank">我的个人博客</a></p>
</footer>

<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/web/assets/js/jquery1.11.1.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/modernizr.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/rem.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/respond.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.legacy.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.min.js"></script>
<!--<![endif]-->
<script src="${pageContext.request.contextPath}/web/assets/js/app.js"></script>
</body>
</html>
