<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>OnlineData</title>
  <meta name="description" content="">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/web/assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/web/assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/admin.css">
  <script src="${pageContext.request.contextPath}/web/assets/js/jquery.min.js"></script>
</head>
<body onload="my()">
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<header class="am-topbar admin-header">
  <div class="am-topbar-brand">
    <strong>OnlineData</strong> <small>在线制图管理系统</small>
  </div>

  <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>
	<!-- 上方标签 -->
	<%@ include file="menuTop.jsp" %>
	<!-- 上方标签 -->
</header>

<div class="am-cf admin-main">
  <!-- sidebar start -->
  <div class="admin-sidebar">
	
	<!-- 左侧导航开始 -->
	<%@ include file="/web/menuLeft.jsp" %>
	<!-- 左侧导航结束-->
    
    <!-- 左下侧广播 -->
    <%@ include file="/web/menuAd.jsp" %>
    <!-- 左下侧广播 -->
  
  <!-- sidebar end -->
<!-- content start -->
  <div class="admin-content">
	<div id="main" style="height:650px"></div>
	<div id="namin2"><button id="testbutton" value="button"></button></div>
  </div>
  
  <script src="http://echarts.baidu.com/build/dist/echarts.js"></script>
<script src="http://echarts.baidu.com/build/dist/echarts.js"></script>
	<script type="text/javascript">
		var jsondata="<s:property value="#session.jsondata"/>";
		jsondata=jsondata.replace(/&quot;/g,'"');
		
		var jsonoption="<s:property value="#session.jsonoption"/>";
		jsonoption=jsonoption.replace(/&quot;/g,'"');
		 
		 
		 var data1=new Array();
		 var data2=new Array();
		 var flag1=0;
		 var flag2=0;
			//配置信息：这六个分别代表了配置信息，从上到下分别为
			//图像名称，单位，图例数组，数据图例书名，值域最大值，值域最小值
			//但是edit页面做5个input就可以了，图例这里可以共用
			//
			var reg = new RegExp('"',"g");
			var ex_text="默认名称";
			var ex_subtext1="默认单位";
			var ex_subtext2="默认单位";
			
			var ex_legend=new Array();
			var ex_series="图例";
			var ex_min="0";
			var ex_max="5000";
			
		function my(){
			var json=JSON.parse(jsondata);
		    console.log(json);
		    var json_option=JSON.parse(jsonoption);
		    for(var i=0;i<json.length;i=i+2)
		    {
		    	data1[flag1++]=json[i].value;
		    }
		    
		    for(var i=1;i<json.length;i=i+2)
		    {
		    	data2[flag2++]=json[i].value;
		    }
		    //这里的问题就是要把双印号都干掉！！！
			ex_text=json_option[0].value;
			ex_subtext1="单位："+json_option[1].value;
			var exname1=json_option[2].value;;
			ex_subtext2="单位："+json_option[3].value;
			var exname2=json_option[4].value;;
			ex_legend[0]=json_option[2].value;
			ex_legend[1]=json_option[4].value;
			//这里的逻辑是先把json对象string化，再去掉string化后的双引号
			
			ex_series=JSON.stringify(json_option[2].value);
			ex_series=ex_series.replace(reg, "")
			
			
			

	
			//console.log(typeof(ex_max));  

			 // 路径配置
		    require.config({
		        paths: {
		            echarts: 'http://echarts.baidu.com/build/dist'
		        }
		    });

		    // 使用
		    require(
		            [
		                'echarts',
		                'echarts/chart/bar', // 使用柱状图就加载bar模块，按需加载
		                'echarts/chart/line',
		                'echarts/chart/pie',
		                'echarts/chart/map',
		                'echarts/chart/gauge',
		            ],
		            function (ec) {
		                // 基于准备好的dom，初始化echarts图表
		                var myChart = ec.init(document.getElementById('main'));

		                var option = {
		                    tooltip : { //提示框，鼠标悬浮交互时的信息提示。
		                        trigger: 'item'
		                        //item与axis的区别
		                        //item只会显示一个。
		                        //而axis会显示所有的数据
		                    },
		                    toolbox: { //工具箱，用来辅助使用，增强功能
		                        show : true,
		                        feature : {
		                            mark : {show: true},
		                            dataView : {show: true, readOnly: false},
		                            magicType: {show: true, type: ['line', 'bar']},
		                            restore : {show: true},
		                            saveAsImage : {show: true}
		                        }
		                    },
		                    //calculable : true, 拖拽特性，我觉得没用
		                    legend: { //图例
		                        data:ex_legend
		                    },
		                    xAxis : [ //X轴
		                        {
		                            type : 'category',
		                            data : ['北京', '天津', '河北', '山西', '内蒙古', '辽宁', '吉林', 
				                            '黑龙江', '上海', '江苏', '浙江', '安徽', '福建', '江西', '山东', 
				                            '河南', '湖北', '湖南', '广东', '广西', '海南', '重庆', 
		                            		'四川', '贵州', '云南', '西藏', '陕西', '甘肃', '青海', '宁夏', '新疆','香港','澳门','台湾']
		                        }
		                    ],
		                    yAxis : [ //Y轴
		                        {
		                            type : 'value',
		                            name : ex_subtext1,
		                            axisLabel : {
		                               
		                            }
		                        },
		                        {
		                            type : 'value',
		                            name : ex_subtext2,

		                        }
		                    ],
		                    series : [ //这个是核心，驱动下面的数组生成图标
		                        {
		                            name:exname1,
		                            type:'bar',
		                            data:data1,
		                        },
		                        {
		                            name:exname2,
		                            type:'line',
		                            yAxisIndex: 1,
		                            data:data2
		                        },

		                    ]
		                };

		                // 为echarts对象加载数据
		                myChart.setOption(option);
		            }
		    );
		}
	</script>
  <!-- content end -->
		
<footer>
  <hr>
  <p class="am-padding-left">© Powered by 汉景源. <a href="http://blog.csdn.net/lucahan" target="_blank">我的个人博客</a></p>
</footer>

<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/web/assets/js/jquery1.11.1.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/modernizr.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/rem.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/respond.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.legacy.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.min.js"></script>
<!--<![endif]-->
<script src="${pageContext.request.contextPath}/web/assets/js/app.js"></script>
</body>
</html>
