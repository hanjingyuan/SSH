<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <ul class="am-list admin-sidebar-list">
      <li><a href="${pageContext.request.contextPath}/web/index.jsp"><span class="am-icon-home"></span> 首页</a></li>
      <li class="admin-parent">
        <a class="am-cf" data-am-collapse="{target: '#left-map'}"><span class="am-icon-calendar"></span> 地图统计图Demo <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
        <ul class="am-list am-collapse admin-sidebar-sub am-in"  id="left-map">
          <li><a href="${pageContext.request.contextPath}/web/content/01-Map.jsp"><span class="am-icon-th"></span> 标准地图</a></li>
          <li><a href="${pageContext.request.contextPath}/web/content/13-Range.jsp"><span class="am-icon-th"></span> 标准地图2</a></li>
          <li><a href="${pageContext.request.contextPath}/web/content/02-MapPie.jsp"><span class="am-icon-th"></span>地图饼图混搭</a></li>
          <li><a href="${pageContext.request.contextPath}/web/content/03-MapBar.jsp"><span class="am-icon-th"></span> 地图柱状图混搭</a></li>
          <li><a href="${pageContext.request.contextPath}/web/content/05-MapLevel.jsp"><span class="am-icon-th"></span> 地图下钻</a></li>
          <li><a href="${pageContext.request.contextPath}/web/content/06-MapMark.jsp"><span class="am-icon-th"></span> 地图标记图</a></li>
        </ul>
        </li>
       <li class="admin-parent">
        <a class="am-cf" data-am-collapse="{target: '#left-charts'}"><span class="am-icon-calendar"></span> 普通统计图Demo <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
        <ul class="am-list am-collapse admin-sidebar-sub" id="left-charts">
          <li><a href="${pageContext.request.contextPath}/web/content/07-Bar.jsp"><span class="am-icon-puzzle-piece"></span>柱状图</a></li>
          <li><a href="${pageContext.request.contextPath}/web/content/08-Line.jsp"><span class="am-icon-puzzle-piece"></span> 折线图</a></li>
          <li><a href="${pageContext.request.contextPath}/web/content/09-Pie.jsp"><span class="am-icon-puzzle-piece"></span> 饼状图</a></li>
          <li><a href="${pageContext.request.contextPath}/web/content/10-BarLine.jsp"><span class="am-icon-puzzle-piece"></span> 柱状折线混搭</a></li>
          <li><a href="${pageContext.request.contextPath}/web/content/11-BarPie.jsp"><span class="am-icon-puzzle-piece"></span>柱状饼状混搭</a></li>
          <li><a href="${pageContext.request.contextPath}/web/content/12-3Mix.jsp"><span class="am-icon-puzzle-piece"></span>多图混搭</a></li>
        </ul>
        </li>
      <li class="admin-parent">
        <a class="am-cf" data-am-collapse="{target: '#left-do'}"><span class="am-icon-pencil-square-o"></span>制图模块 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
        <ul class="am-list am-collapse admin-sidebar-sub" id="left-do">
          <li><a href="${pageContext.request.contextPath}/web/Edit01Map.jsp" class="am-cf"><span class="am-icon-check"></span>地图统计图制作</a></li>
          <li><a href="${pageContext.request.contextPath}/web/Edit02MapPie.jsp" class="am-cf"><span class="am-icon-check"></span>地图饼图统计图制作</a></li>
          <li><a href="${pageContext.request.contextPath}/web/Edit03MapBar.jsp" class="am-cf"><span class="am-icon-check"></span>地图柱状图制作</a></li>
          <li><a href="${pageContext.request.contextPath}/web/Edit04MapLev.jsp" class="am-cf"><span class="am-icon-check"></span>地图下钻图制作</a></li>
          <li><a href="${pageContext.request.contextPath}/web/Edit05Line.jsp" class="am-cf"><span class="am-icon-check"></span>折线图制作</a></li>
          <li><a href="${pageContext.request.contextPath}/web/Edit06Bar.jsp" class="am-cf"><span class="am-icon-check"></span>柱状图制作</a></li>
          <li><a href="${pageContext.request.contextPath}/web/Edit07Pie.jsp" class="am-cf"><span class="am-icon-check"></span>饼状图制作</a></li>
          <li><a href="${pageContext.request.contextPath}/web/Edit08BarLine.jsp" class="am-cf"><span class="am-icon-check"></span>饼状折线图制作</a></li>
        </ul>
      </li>
      
        <li class="admin-parent">
        <a class="am-cf" data-am-collapse="{target: '#left-help'}"><span class="am-icon-file"></span> 帮助模块<span class="am-icon-angle-right am-fr am-margin-right"></span></a>
        <ul class="am-list am-collapse admin-sidebar-sub" id="left-help">
          <li><a href="${pageContext.request.contextPath}/web/Help1.jsp"><span class="am-icon-puzzle-help"></span>使用概述</a></li>
          <li><a href="${pageContext.request.contextPath}/web/Help2.jsp"><span class="am-icon-puzzle-help"></span>地图统计图说明</a></li>
          <li><a href="${pageContext.request.contextPath}/web/Help3.jsp"><span class="am-icon-puzzle-help"></span>普通统计图说明</a></li>
          <li><a href="${pageContext.request.contextPath}/web/Help4.jsp"><span class="am-icon-puzzle-help"></span>制图模块使用帮助</a></li>
        </ul>
        </li>
      <li><a href="#"><span class="am-icon-sign-out"></span> 离开</a></li>
    </ul>