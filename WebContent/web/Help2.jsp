<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>OnlineData</title>
  <meta name="description" content="">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/web/assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/web/assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/admin.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<header class="am-topbar admin-header">
  <div class="am-topbar-brand">
    <strong>OnlineData</strong> <small>在线制图网站</small>
  </div>

  <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>
	<!-- 上方标签 -->
	<%@ include file="menuTop.jsp" %>
	<!-- 上方标签 -->
</header>

<div class="am-cf admin-main">
  <!-- sidebar start -->
  <div class="admin-sidebar">
	
	<!-- 左侧导航开始 -->
	<%@ include file="/web/menuLeft.jsp" %>
	<!-- 左侧导航结束-->
    
    <!-- 左下侧广播 -->
    <%@ include file="/web/menuAd.jsp" %>
    <!-- 左下侧广播 -->
  
  <!-- sidebar end -->
<!-- content start -->
  <div class="admin-content" >
	 <div class="admin-content-body">
      <div class="am-cf am-padding am-padding-bottom-0">
        <div class="am-fl am-cf">
          <strong class="am-text-primary am-text-lg">地图统计图说明</strong> /
          <small>Help</small>
        </div>
      </div>
      <hr/>
      <div class="am-g">
        <div class="am-u-sm-12 am-u-sm-centered">
	          <div id="main">
	          	  <h1>二、	地图统计图DEMO说明</h1>
	          	  <p>地图统计图DEMO中包含了标准地图，标准地图2，地图饼图混搭，地图柱状图混搭，地图下钻和地图标记图共计6种不同的DEMO示例。用户可以查看不同的DEMO，找出自己适合的DEMO来进行相应的制图操作。</p>
	          	  <p>同时，在后期的维护过程中，管理员还可以陆续添加更多的DEMO示例，这样的话，用户在使用的过程中，就可以用更多选择来更好地完成制图需求。而这也是制图工具的目的。</p>
	          	  <p>接下来，进行地图统计图DEMO下面各个示例的介绍。</p>
	          	  <hr>
	          	  <h2>1.	标准地图</h2>
	          	  <p>标准地图包含了主标题，数值单位，图例显示，值域漫游，数据视图查看，漫游缩放组件，保存为图片。</p>
	          	  <p>在标准地图中，每一个省份的数据以不同颜色的深浅来进行展示，用户在使用的时候，可以直接通过观察颜色深浅的不同，可以直接看出每个省份数据的高低不同。用户还可以通过移动鼠标到不同的省份上，可以直接看到每个省份的数据，方便，快速。</p>
	          	  <p>用户也可以使用左侧的值域漫游按钮，通过拖拽实现展示不同省份的值域变化。</p>
	          	  <p>统计图的数据同时支持在线的更改，用户可以通过点击右侧的数据视图，可以查看到当前每个省份的数据信息，用户可以在这里，修改不同省份的数据，更改完成之后，可以直接点击下方的刷新按钮，就完成了数据的刷新。</p>
	          	  <p>如果用户需要将统计图在PPT或者Word文档中使用，可以点击右侧的保存为图片按钮，当前统计图可以另存为PNG格式的图片，这样用户就可以方便快捷的在各种不同的场景中使用统计图了，不会仅仅局限于在线网页浏览的方式。</p>
	          	  <hr>
	          	  <h2>2.	标准地图2</h2>
	          	  <p>标准地图2包含了主标题，数值单位，图例显示，值域漫游，数据视图查看，漫游缩放组件，保存为图片。与标准地图1不同的时候，标准地图2采用的是自定义的颜色，而标准地图采用的是数据从小到大，颜色从浅变深。</p>
	          	  <p>在标准地图2中，每一个省份的数据以不同颜色的深浅来进行展示，用户在使用的时候，可以直接通过观察颜色深浅的不同，可以直接看出每个省份数据的高低不同。用户还可以通过移动鼠标到不同的省份上，可以直接看到每个省份的数据，方便，快速。</p>
	          	  <p>用户也可以使用左侧的值域漫游按钮，通过拖拽实现展示不同省份的值域变化。</p>
	          	  <p>统计图的数据同时支持在线的更改，用户可以通过点击右侧的数据视图，可以查看到当前每个省份的数据信息，用户可以在这里，修改不同省份的数据，更改完成之后，可以直接点击下方的刷新按钮，就完成了数据的刷新。</p>
	          	  <p>如果用户需要将统计图在PPT或者Word文档中使用，可以点击右侧的保存为图片按钮，当前统计图可以另存为PNG格式的图片，这样用户就可以方便快捷的在各种不同的场景中使用统计图了，不会仅仅局限于在线网页浏览的方式。</p>
	          	  <p></p>	          	  
	          	  <hr>
	          	   <h2>3.	地图饼图混搭</h2>
	          	  <p>地图饼图混搭包含了主标题，数值单位，值域漫游，数据视图查看，漫游缩放组件，保存为图片，饼图比例展示。</p>
	          	  <p>在地图饼图混搭中，每一个省份的数据以不同颜色的深浅来进行展示，用户在使用的时候，可以直接通过观察颜色深浅的不同，可以直接看出每个省份数据的高低不同。用户还可以通过移动鼠标到不同的省份上，可以直接看到每个省份的数据，方便，快速。</p>
	          	  <p>用户也可以使用左侧的值域漫游按钮，通过拖拽实现展示不同省份的值域变化。</p>
	          	  <p>在地图饼图混搭中，用户可以点击不同省份，这样会在右侧动态生成一个饼图，这样，用户可以直观的感受到所选数据的大小比较，以及具体的所占比例。</p>
	          	  <p>统计图的数据同时支持在线的更改，用户可以通过点击右侧的数据视图，可以查看到当前每个省份的数据信息，用户可以在这里，修改不同省份的数据，更改完成之后，可以直接点击下方的刷新按钮，就完成了数据的刷新。</p>
	          	  <p>如果用户需要将统计图在PPT或者Word文档中使用，可以点击右侧的保存为图片按钮，当前统计图可以另存为PNG格式的图片，这样用户就可以方便快捷的在各种不同的场景中使用统计图了，不会仅仅局限于在线网页浏览的方式。</p>
	          	  <hr>
	          	  <h2>4.	地图柱状图混搭</h2>
	          	  <p>标准地图包含了主标题，数值单位，图例显示，值域漫游，数据视图查看，漫游缩放组件，保存为图片，柱状图折线图转换。</p>
	          	  <p>在地图柱状图混搭中，每一个省份的数据以不同颜色的深浅来进行展示，用户在使用的时候，可以直接通过观察颜色深浅的不同，可以直接看出每个省份数据的高低不同。用户还可以通过移动鼠标到不同的省份上，可以直接看到每个省份的数据，方便，快速。同时，在地图的左侧为数据的柱状图，柱状图和地图同时存在，可以直观的查看数据的在柱状图中和地图统计图中不同的表现形式。</p>
	          	  <p>用户在使用柱状图时，可以通过点击上部的折线图按钮，轻松实现柱状图和折线图的快读转换。</p>
	          	  <p>用户也可以使用左侧的值域漫游按钮，通过拖拽实现展示不同省份的值域变化。</p>
	          	  <p>统计图的数据同时支持在线的更改，用户可以通过点击右侧的数据视图，可以查看到当前每个省份的数据信息，用户可以在这里，修改不同省份的数据，更改完成之后，可以直接点击下方的刷新按钮，就完成了数据的刷新。</p>
	          	  <p>如果用户需要将统计图在PPT或者Word文档中使用，可以点击右侧的保存为图片按钮，当前统计图可以另存为PNG格式的图片，这样用户就可以方便快捷的在各种不同的场景中使用统计图了，不会仅仅局限于在线网页浏览的方式。</p>
	          	  	  <hr>        	  
	          	   <h2>5.	地图下钻图</h2>
	          	  <p>地图下钻图与之前的统计图存在明显的不同。地图下钻图是指的点击不同的省份，则地图进入到省份之中。</p>
	          	  <p>例如点击广东省，则进入广东省之中，此时显示的是已经广东省中各个地级市的数据。</p>
	          	  <p>这个统计图的特点是节省了空间，同时完成了全国所有地级市的数据的展示。</p>
	          	  <p>地图下钻图支持保存为图片，但是需要注意的是，由于地图下钻图的全国图是没有保存意义的，所以建议用户保存分省数据图作为统计图。</p>	          	  
	          	  <hr>
	          	  <h2>6.	地图标记图</h2>
	          	  <p>地图标记图包含了主标题，数值单位，图例显示，值域漫游，数据视图查看，漫游缩放组件，保存为图片。</p>
	          	  <p>在地图标记图中，展示的是城市的信息，统计图中，展示的是点数据，根据每个城市的数据的不同，各个城市点的颜色深度也是不同的。地图标记图的主要作用就是用来展示城市的数据的不同。</p>
	          	  <p>用户也可以使用左侧的值域漫游按钮，通过拖拽实现展示不同省份的值域变化。</p>
	          	  <p>统计图的数据同时支持在线的更改，用户可以通过点击右侧的数据视图，可以查看到当前每个省份的数据信息，用户可以在这里，修改不同省份的数据，更改完成之后，可以直接点击下方的刷新按钮，就完成了数据的刷新。</p>
	          	  <p>如果用户需要将统计图在PPT或者Word文档中使用，可以点击右侧的保存为图片按钮，当前统计图可以另存为PNG格式的图片，这样用户就可以方便快捷的在各种不同的场景中使用统计图了，不会仅仅局限于在线网页浏览的方式。</p>
	          	  <p></p>
	          	  	          		          
	          </div>
        </div>
      </div>
      </div>
  </div>
  <!-- content end -->
		


<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/web/assets/js/jquery1.11.1.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/modernizr.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/rem.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/respond.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.legacy.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="${pageContext.request.contextPath}/web/assets/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.min.js"></script>
<!--<![endif]-->
<script src="${pageContext.request.contextPath}/web/assets/js/app.js"></script>
</body>
</html>
