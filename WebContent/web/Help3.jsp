<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>OnlineData</title>
  <meta name="description" content="">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/web/assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/web/assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/admin.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<header class="am-topbar admin-header">
  <div class="am-topbar-brand">
    <strong>OnlineData</strong> <small>在线制图网站</small>
  </div>

  <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>
	<!-- 上方标签 -->
	<%@ include file="menuTop.jsp" %>
	<!-- 上方标签 -->
</header>

<div class="am-cf admin-main">
  <!-- sidebar start -->
  <div class="admin-sidebar">
	
	<!-- 左侧导航开始 -->
	<%@ include file="/web/menuLeft.jsp" %>
	<!-- 左侧导航结束-->
    
    <!-- 左下侧广播 -->
    <%@ include file="/web/menuAd.jsp" %>
    <!-- 左下侧广播 -->
  
  <!-- sidebar end -->
<!-- content start -->
  <div class="admin-content" >
	 <div class="admin-content-body">
      <div class="am-cf am-padding am-padding-bottom-0">
        <div class="am-fl am-cf">
          <strong class="am-text-primary am-text-lg">普通统计图说明</strong> /
          <small>Help</small>
        </div>
      </div>
      <hr/>
      <div class="am-g">
        <div class="am-u-sm-12 am-u-sm-centered">
	          <div id="main" >
	          	  <h1>三、	普通统计图DEMO说明</h1>
	          	  <p>除了上面的地图统计图以外，还增加了我们平常常用的统计图形式，来满足不同用户的多种需求，普通统计图DEMO中，包括了柱状图，折线图，饼状图，柱状折线混搭，柱状饼状混搭，多图混搭，共计6种不同的普通统计图。</p>
	          	  <p>通过多种不同的统计图风格，来完善在线制图系统，满足不同用户对于不同的在线制图的需求。</p>
	          	  
	          	  <hr>	          	  
	          	  <h2>1.	柱状图</h2>
	          	  <p>柱状图包括了标题，单位，图例，柱状折线转换，数据视图，保存为图片。</p>
	          	  <p>柱状图(bar chart)，是一种以长方形的长度为变量的表达图形的统计报告图，由一系列高度不等的纵向条纹表示数据分布的情况，用来比较两个或以上的价值（不同时间或者不同条件），只有一个变量，通常利用于较小的数据集分析。柱状图亦可横向排列，或用多维方式表达。</p>
	          	  <p>与上面的地图统计图基本相似，柱状图可以进行数据的即时更改，另存为图片等一系列的操作，方便用户快捷的使用统计图。</p>
	          	  <p></p>
	          	  <p></p>
	          	  
	          	  <hr>	          	  
	          	  <h2>2.	折线图</h2>
	          	  <p>折线图与柱状图配置基本相似，除了拥有柱状图的基本功能以外，还包括了显示最大值，最小值和平均值。</p>
	          	  <p>排列在工作表的列或行中的数据可以绘制到折线图中。折线图可以显示随时间（根据常用比例设置）而变化的连续数据，因此非常适用于显示在相等时间间隔下数据的趋势。在折线图中，类别数据沿水平轴均匀分布，所有值数据沿垂直轴均匀分布。</p>
	          	  <p></p>
	          	  <p></p>
	          	  <p></p>
	          	  
	          	  <hr>	          	  
	          	  <h2>3.	饼状图</h2>
	          	  <p>饼状图采用了数据和百分比混合的模式。也就是说数据和百分比都可以显示，可以极大的方便用户对于饼状图的使用。</p>
	          	  <p>饼状图显示一个数据系列（数据系列：在图表中绘制的相关数据点，这些数据源自数据表的行或列。图表中的每个数据系列具有唯一的颜色或图案并且在图表的图例中表示。可以在图表中绘制一个或多个数据系列。饼状图只有一个数据系列。）中各项的大小与各项总和的比例。饼状图中的数据点（数据点：在图表中绘制的单个值，这些值由条形、柱形、折线、饼状图或圆环图的扇面、圆点和其他被称为数据标记的图形表示。相同颜色的数据标记组成一个数据系列。）显示为整个饼状图的百分比。</p>
	          	  <p></p>
	          	  <p></p>
	          	  <p></p>
	          	  
	          	  <hr>	          	  
	          	  <h2>4.	柱状折线混搭</h2>
	          	  <p>柱状图折线混搭图结合了柱状图和折线图，使得两张图在同一张图中显示。</p>
	          	  <p></p>
	          	  <p></p>
	          	  <p></p>
	          	  <p></p>
	          	  
	          	  <hr>	          	  
	          	  <h2>5.	柱状饼状混搭</h2>
	          	  <p>柱状饼状混搭结合了柱状图和饼状图，使得两张图在同一张图中显示。</p>
	          	  <p></p>
	          	  <p></p>
	          	  <p></p>
	          	  <p></p>
	          	  
	          	  <hr>	          	  
	          	  <h2>6.	多图混搭</h2>
	          	  <p>多图混搭图是一款非常炫丽的统计图，它结合了柱状图，折线图，饼状图三图混合。用户可以根据自身的需求来定制所需要的图像。</p>
	          	  <p></p>
	          	  <p></p>
	          	  <p></p>
	          	  <p></p>	          	 	          	  	          	  	          	  	          	  	          	  
	          </div>
        </div>
      </div>
      </div>
  </div>
  <!-- content end -->
		


<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/web/assets/js/jquery1.11.1.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/modernizr.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/rem.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/respond.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.legacy.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="${pageContext.request.contextPath}/web/assets/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.min.js"></script>
<!--<![endif]-->
<script src="${pageContext.request.contextPath}/web/assets/js/app.js"></script>
</body>
</html>
