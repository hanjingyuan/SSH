<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <title>模板</title>
    <!--示例网址-->
    <!--中文介绍-->
    <!---->
      <script src="${pageContext.request.contextPath}/web/assets/js/jquery.min.js"></script>
</head>
<body>
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<div id="main" style="height:650px;width:50%"></div>
<div id="main2" style="height:650px;width:50%">
<form id="data">
    <table border="1">
        <tr>
            <th>名称</th>

        </tr>
        <tr>
            <td>January</td>

        </tr>
        <tr>
        	<td>1.北京</td>
            <td><input type="text" name="bejing1" value="1000" ></td>
        </tr>
        <tr>
        	<td>2.天津</td>
            <td><input type="text" name="tianjin1" value="1000" ></td>
        </tr>
        <tr>
        	<td>3.河北</td>
            <td><input type="text" name="hebei1" value="1000" ></td>
        </tr>
        <tr>
        	<td>4.上海</td>
            <td><input type="text" name="shanxi1" value="1000" ></td>
        </tr>
        <tr>
        	<td>5.内蒙古</td>
            <td><input type="text" name="neimenggu1" value="1000" ></td>
        </tr>
        <tr>
        	<td>6.辽宁</td>
            <td><input type="text" name="liaoning1" value="1000" ></td>
        </tr>
        <tr>
        	<td>7.吉林</td>
            <td><input type="text" name="jilin1" value="1000" ></td>
        </tr>
        <tr>
        	<td>8.黑龙江</td>
            <td><input type="text" name="heilongjiang1" value="1000" ></td>
        </tr>
        <tr>
        	<td>9.上海</td>
            <td><input type="text" name="shanghai1" value="1000" ></td>
        </tr>
        <tr>
        	<td>10.江苏</td>
            <td><input type="text" name="jiangsu1" value="1000" ></td>
        </tr>
        <tr>
        	<td>11.浙江</td>
            <td><input type="text" name="zhejiang1" value="1000" ></td>
        </tr>
        <tr>
        	<td>12.安徽</td>
            <td><input type="text" name="anhui1" value="1000" ></td>
        </tr>
        <tr>
        	<td>13.福建</td>
            <td><input type="text" name="fujian1" value="1000" ></td>
        </tr>
        <tr>
        	<td>14.江西</td>
            <td><input type="text" name="jiangxi1" value="1000" ></td>
        </tr>
        <tr>
        	<td>15.山东</td>
            <td><input type="text" name="shandong1" value="1000" ></td>
        </tr>
        <tr>
        	<td>16.河南</td>
            <td><input type="text" name="henan1" value="1000" ></td>
        </tr>
        <tr>
        	<td>17.湖北</td>
            <td><input type="text" name="hubei1" value="1000" ></td>
        </tr>
        <tr>
        	<td>18.湖南</td>
            <td><input type="text" name="hunan1" value="1000" ></td>
        </tr>
        <tr>
        	<td>19.广东</td>
            <td><input type="text" name="guangdong1" value="1000" ></td>
        </tr>
        <tr>
        	<td>20.广西</td>
            <td><input type="text" name="guangxi1" value="1000" ></td>
        </tr>
        <tr>
        	<td>21.海南</td>
            <td><input type="text" name="hainan1" value="1000" ></td>
        </tr>
        <tr>
        	<td>22.重庆</td>
            <td><input type="text" name="chongqing1" value="1000" ></td>
        </tr>
        <tr>
        	<td>23.四川</td>
            <td><input type="text" name="sichuang1" value="1000" ></td>
        </tr>
        <tr>
        	<td>24.贵州</td>
            <td><input type="text" name="guizhou1" value="1000" ></td>
        </tr>
        <tr>
        	<td>25.云南</td>
            <td><input type="text" name="yunnan1" value="1000" ></td>
        </tr>
        <tr>
        	<td>26.西藏</td>
            <td><input type="text" name="xizang1" value="1000" ></td>
        </tr>
        <tr>
        	<td>27.陕西</td>
            <td><input type="text" name="shanxi1" value="1000" ></td>
        </tr>
        <tr>
        	<td>28.甘肃</td>
            <td><input type="text" name="gansu1" value="1000" ></td>
        </tr>
        <tr>
        	<td>29.青海</td>
            <td><input type="text" name="qinghai1" value="1000" ></td>
        </tr>
        <tr>
        	<td>30.宁夏</td>
            <td><input type="text" name="ningxia1" value="1000" ></td>
        </tr>
        <tr>
        	<td>31.新疆</td>
            <td><input type="text" name="xinjiang1" value="1000" ></td>
        </tr>
        <tr>
        	<td>32.香港</td>
            <td><input type="text" name="xianggang1" value="1000" ></td>
        </tr>
        
        <tr>
        	<td>33.澳门</td>
            <td><input type="text" name="aomen1" value="1000" ></td>
        </tr>
                
        <tr>
        	<td>34.台湾</td>
            <td><input type="text" name="taiwan1" value="1000" ></td>
        </tr>
    </table>
    <input type="button" value="转换" onclick="my()">

</form>
    <div id="main3"></div>
	    <form action="${pageContext.request.contextPath}/category_test.action" method="post">
	    	<input type="text" id="main4" name="jsondata"></input>
	    	<input type="submit" value="提交">
    </form>
</div>
<!-- ECharts单文件引入 -->
<script src="http://echarts.baidu.com/build/dist/echarts.js"></script>
<script type="text/javascript">
var ex= [
         {
             "name": "北京",
             "value": "5"
         },
         {
             "name": "天津",
             "value": "5"
         },
         {
             "name": "河北",
             "value": "9"
         },
         {
             "name": "山西",
             "value": "9"
         },
         {
             "name": "内蒙古",
             "value": "9"
         },
         {
             "name": "辽宁",
             "value": "9"
         },
         {
             "name": "吉林",
             "value": "9"
         },
         {
             "name": "黑龙江",
             "value": "9"
         },
         {
             "name": "上海",
             "value": "9"
         },
         {
             "name": "江苏",
             "value": "9"
         },
         {
             "name": "浙江",
             "value": "9"
         },
         {
             "name": "安徽",
             "value": "9"
         },
         {
             "name": "福建",
             "value": "9"
         },
         {
             "name": "江西",
             "value": "9"
         },
         {
             "name": "山东",
             "value": "9"
         },
         {
             "name": "河南",
             "value": "9"
         },
         {
             "name": "湖北",
             "value": "9"
         },
         {
             "name": "湖南",
             "value": "9"
         },
         {
             "name": "广东",
             "value": "9"
         },
         {
             "name": "广西",
             "value": "9"
         },
         {
             "name": "海南",
             "value": "9"
         },
         {
             "name": "重庆",
             "value": "9"
         },
         {
             "name": "四川",
             "value": "9"
         },
         {
             "name": "贵州",
             "value": "9"
         },
         {
             "name": "云南",
             "value": "9"
         },
         {
             "name": "西藏",
             "value": "9"
         },
         {
             "name": "陕西",
             "value": "9"
         },
         {
             "name": "甘肃",
             "value": "190"
         },
         {
             "name": "青海",
             "value": "1900"
         },
         {
             "name": "宁夏",
             "value": "900"
         },
         {
             "name": "新疆",
             "value": "5000"
         }
     ]
	function my(){
		//将表单数据序列化
	    var jsondata=JSON.stringify($("#data").serializeArray());
	    document.getElementById("main3").innerHTML=jsondata;
	    document.getElementById("main4").value=jsondata;
	    
	    function show(result){
	    	//通过将得到的字符串，转换为json对象进行操作
	    	var json2=JSON.parse(result);
	        console.log(json2);
	        ex[27].value=4500;
	        console.log(ex);
	    }
	}	
    // 路径配置
    require.config({
        paths: {
            echarts: 'http://echarts.baidu.com/build/dist'
        }
    });

    // 使用
    require(
            [
                'echarts',
                'echarts/chart/bar', // 使用柱状图就加载bar模块，按需加载
                'echarts/chart/line',
                'echarts/chart/pie',
                'echarts/chart/map',
                'echarts/chart/gauge',
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main'));

                var option = {
                    title : {
                        text: '全国水资源情况',
                        subtext: '2010年',
                        x:'center'
                    },
                    tooltip : {
                        trigger: 'item',
                        formatter: "{a} <br/>{b}  :{c}亿立方米 "
                    },
                    legend: {
                        orient: 'vertical',
                        x:'left',
                        data:['水资源总量']
                    },
                    dataRange: {
                        min: 0,
                        max: 5000,
                        x: 'left',
                        y: 'bottom',
                        text:['高','低'],           // 文本，默认为数值文本
                        calculable : true
                    },
                    toolbox: {
                        show: true,
                        orient : 'vertical',
                        x: 'right',
                        y: 'center',
                        feature : {
                            mark : {show: true},
                            dataView : {show: true, readOnly: false},
                            restore : {show: true},
                            saveAsImage : {show: true}
                        }
                    },
                    roamController: {
                        show: true,
                        x: 'right',
                        mapTypeControl: {
                            'china': true
                        }
                    },
                    series : [
                        {
                            name: '水资源总量',
                            type: 'map',
                            mapType: 'china',
                            roam: false,
                            itemStyle:{
                                normal:{label:{show:true}},
                                emphasis:{label:{show:true}}
                            },
                            data:ex
                        }                                       
                    ]
                };
                // 为echarts对象加载数据
                myChart.setOption(option);
            }
    );
</script>
</body>
</html>