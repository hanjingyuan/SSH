<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>OnlineData</title>
  <meta name="description" content="">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/web/assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/web/assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/admin.css">
  <script src="${pageContext.request.contextPath}/web/assets/js/jquery.min.js"></script>
  <style type="text/css">
       body{margin: 0;padding: 0;}
       #main{width: 100%;height: 800px;margin: 0 auto;float: left;}
       
    </style>
</head>
<body onload="my()">
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<header class="am-topbar admin-header">
  <div class="am-topbar-brand">
    <strong>OnlineData</strong> <small>在线制图管理系统</small>
  </div>

  <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>
	<!-- 上方标签 -->
	<%@ include file="menuTop.jsp" %>
	<!-- 上方标签 -->
</header>

<div class="am-cf admin-main">
  <!-- sidebar start -->
  <div class="admin-sidebar">
	
	<!-- 左侧导航开始 -->
	<%@ include file="/web/menuLeft.jsp" %>
	<!-- 左侧导航结束-->
    
    <!-- 左下侧广播 -->
    <%@ include file="/web/menuAd.jsp" %>
    <!-- 左下侧广播 -->
  
  <!-- sidebar end -->
<!-- content start -->
  <div class="admin-content">
	<div id="main" style="height:800px" ></div>
	<div id="main2" style="height:800px"></div>
	
  </div>
  
  <script src="http://echarts.baidu.com/build/dist/echarts.js"></script>
<script src="http://echarts.baidu.com/build/dist/echarts.js"></script>
	<script type="text/javascript">
		var jsondata="<s:property value="#session.jsondata"/>";
		jsondata=jsondata.replace(/&quot;/g,'"');
		
		var jsonoption="<s:property value="#session.jsonoption"/>";
		jsonoption=jsonoption.replace(/&quot;/g,'"');
		 
		 
		 var ex=[
                 {name: '重庆市',value: 50},
                 {name: '北京市',value: 50},
                 {name: '天津市',value: 50},
                 {name: '上海市',value: 50},
                 {name: '香港',value: 50},
                 {name: '澳门',value: 50},
                 {name: '巴音郭楞蒙古自治州',value: 50},
                 {name: '和田地区',value: 50},
                 {name: '哈密地区',value: 50},
                 {name: '阿克苏地区',value: 50},
                 {name: '阿勒泰地区',value: 50},
                 {name: '喀什地区',value: 50},
                 {name: '塔城地区',value: 50},
                 {name: '昌吉回族自治州',value: 50},
                 {name: '克孜勒苏柯尔克孜自治州',value: 50},
                 {name: '吐鲁番地区',value: 50},
                 {name: '伊犁哈萨克自治州',value: 50},
                 {name: '博尔塔拉蒙古自治州',value: 50},
                 {name: '乌鲁木齐市',value: 50},
                 {name: '克拉玛依市',value: 50},
                 {name: '阿拉尔市',value: 50},
                 {name: '图木舒克市',value: 50},
                 {name: '五家渠市',value: 50},
                 {name: '石河子市',value: 50},
                 {name: '那曲地区',value: 50},
                 {name: '阿里地区',value: 50},
                 {name: '日喀则地区',value: 50},
                 {name: '林芝地区',value: 50},
                 {name: '昌都地区',value: 50},
                 {name: '山南地区',value: 50},
                 {name: '拉萨市',value: 50},
                 {name: '呼伦贝尔市',value: 50},
                 {name: '阿拉善盟',value: 50},
                 {name: '锡林郭勒盟',value: 50},
                 {name: '鄂尔多斯市',value: 50},
                 {name: '赤峰市',value: 50},
                 {name: '巴彦淖尔市',value: 50},
                 {name: '通辽市',value: 50},
                 {name: '乌兰察布市',value: 50},
                 {name: '兴安盟',value: 50},
                 {name: '包头市',value: 50},
                 {name: '呼和浩特市',value: 50},
                 {name: '乌海市',value: 50},
                 {name: '海西蒙古族藏族自治州',value: 50},
                 {name: '玉树藏族自治州',value: 50},
                 {name: '果洛藏族自治州',value: 50},
                 {name: '海南藏族自治州',value: 50},
                 {name: '海北藏族自治州',value: 50},
                 {name: '黄南藏族自治州',value: 50},
                 {name: '海东地区',value: 50},
                 {name: '西宁市',value: 50},
                 {name: '甘孜藏族自治州',value: 50},
                 {name: '阿坝藏族羌族自治州',value: 50},
                 {name: '凉山彝族自治州',value: 50},
                 {name: '绵阳市',value: 50},
                 {name: '达州市',value: 50},
                 {name: '广元市',value: 50},
                 {name: '雅安市',value: 50},
                 {name: '宜宾市',value: 50},
                 {name: '乐山市',value: 50},
                 {name: '南充市',value: 50},
                 {name: '巴中市',value: 50},
                 {name: '泸州市',value: 50},
                 {name: '成都市',value: 50},
                 {name: '资阳市',value: 50},
                 {name: '攀枝花市',value: 50},
                 {name: '眉山市',value: 50},
                 {name: '广安市',value: 50},
                 {name: '德阳市',value: 50},
                 {name: '内江市',value: 50},
                 {name: '遂宁市',value: 50},
                 {name: '自贡市',value: 50},
                 {name: '黑河市',value: 50},
                 {name: '大兴安岭地区',value: 50},
                 {name: '哈尔滨市',value: 50},
                 {name: '齐齐哈尔市',value: 50},
                 {name: '牡丹江市',value: 50},
                 {name: '绥化市',value: 50},
                 {name: '伊春市',value: 50},
                 {name: '佳木斯市',value: 50},
                 {name: '鸡西市',value: 50},
                 {name: '双鸭山市',value: 50},
                 {name: '大庆市',value: 50},
                 {name: '鹤岗市',value: 50},
                 {name: '七台河市',value: 50},
                 {name: '酒泉市',value: 50},
                 {name: '张掖市',value: 50},
                 {name: '甘南藏族自治州',value: 50},
                 {name: '武威市',value: 50},
                 {name: '陇南市',value: 50},
                 {name: '庆阳市',value: 50},
                 {name: '白银市',value: 50},
                 {name: '定西市',value: 50},
                 {name: '天水市',value: 50},
                 {name: '兰州市',value: 50},
                 {name: '平凉市',value: 50},
                 {name: '临夏回族自治州',value: 50},
                 {name: '金昌市',value: 50},
                 {name: '嘉峪关市',value: 50},
                 {name: '普洱市',value: 50},
                 {name: '红河哈尼族彝族自治州',value: 50},
                 {name: '文山壮族苗族自治州',value: 50},
                 {name: '曲靖市',value: 50},
                 {name: '楚雄彝族自治州',value: 50},
                 {name: '大理白族自治州',value: 50},
                 {name: '临沧市',value: 50},
                 {name: '迪庆藏族自治州',value: 50},
                 {name: '昭通市',value: 50},
                 {name: '昆明市',value: 50},
                 {name: '丽江市',value: 50},
                 {name: '西双版纳傣族自治州',value: 50},
                 {name: '保山市',value: 50},
                 {name: '玉溪市',value: 50},
                 {name: '怒江傈僳族自治州',value: 50},
                 {name: '德宏傣族景颇族自治州',value: 50},
                 {name: '百色市',value: 50},
                 {name: '河池市',value: 50},
                 {name: '桂林市',value: 50},
                 {name: '南宁市',value: 50},
                 {name: '柳州市',value: 50},
                 {name: '崇左市',value: 50},
                 {name: '来宾市',value: 50},
                 {name: '玉林市',value: 50},
                 {name: '梧州市',value: 50},
                 {name: '贺州市',value: 50},
                 {name: '钦州市',value: 50},
                 {name: '贵港市',value: 50},
                 {name: '防城港市',value: 50},
                 {name: '北海市',value: 50},
                 {name: '怀化市',value: 50},
                 {name: '永州市',value: 50},
                 {name: '邵阳市',value: 50},
                 {name: '郴州市',value: 50},
                 {name: '常德市',value: 50},
                 {name: '湘西土家族苗族自治州',value: 50},
                 {name: '衡阳市',value: 50},
                 {name: '岳阳市',value: 50},
                 {name: '益阳市',value: 50},
                 {name: '长沙市',value: 50},
                 {name: '株洲市',value: 50},
                 {name: '张家界市',value: 50},
                 {name: '娄底市',value: 50},
                 {name: '湘潭市',value: 50},
                 {name: '榆林市',value: 50},
                 {name: '延安市',value: 50},
                 {name: '汉中市',value: 50},
                 {name: '安康市',value: 50},
                 {name: '商洛市',value: 50},
                 {name: '宝鸡市',value: 50},
                 {name: '渭南市',value: 50},
                 {name: '咸阳市',value: 50},
                 {name: '西安市',value: 50},
                 {name: '铜川市',value: 50},
                 {name: '清远市',value: 50},
                 {name: '韶关市',value: 50},
                 {name: '湛江市',value: 50},
                 {name: '梅州市',value: 50},
                 {name: '河源市',value: 50},
                 {name: '肇庆市',value: 50},
                 {name: '惠州市',value: 50},
                 {name: '茂名市',value: 50},
                 {name: '江门市',value: 50},
                 {name: '阳江市',value: 50},
                 {name: '云浮市',value: 50},
                 {name: '广州市',value: 50},
                 {name: '汕尾市',value: 50},
                 {name: '揭阳市',value: 50},
                 {name: '珠海市',value: 50},
                 {name: '佛山市',value: 50},
                 {name: '潮州市',value: 50},
                 {name: '汕头市',value: 50},
                 {name: '深圳市',value: 50},
                 {name: '东莞市',value: 50},
                 {name: '中山市',value: 50},
                 {name: '延边朝鲜族自治州',value: 50},
                 {name: '吉林市',value: 50},
                 {name: '白城市',value: 50},
                 {name: '松原市',value: 50},
                 {name: '长春市',value: 50},
                 {name: '白山市',value: 50},
                 {name: '通化市',value: 50},
                 {name: '四平市',value: 50},
                 {name: '辽源市',value: 50},
                 {name: '承德市',value: 50},
                 {name: '张家口市',value: 50},
                 {name: '保定市',value: 50},
                 {name: '唐山市',value: 50},
                 {name: '沧州市',value: 50},
                 {name: '石家庄市',value: 50},
                 {name: '邢台市',value: 50},
                 {name: '邯郸市',value: 50},
                 {name: '秦皇岛市',value: 50},
                 {name: '衡水市',value: 50},
                 {name: '廊坊市',value: 50},
                 {name: '恩施土家族苗族自治州',value: 50},
                 {name: '十堰市',value: 50},
                 {name: '宜昌市',value: 50},
                 {name: '襄樊市',value: 50},
                 {name: '黄冈市',value: 50},
                 {name: '荆州市',value: 50},
                 {name: '荆门市',value: 50},
                 {name: '咸宁市',value: 50},
                 {name: '随州市',value: 50},
                 {name: '孝感市',value: 50},
                 {name: '武汉市',value: 50},
                 {name: '黄石市',value: 50},
                 {name: '神农架林区',value: 50},
                 {name: '天门市',value: 50},
                 {name: '仙桃市',value: 50},
                 {name: '潜江市',value: 50},
                 {name: '鄂州市',value: 50},
                 {name: '遵义市',value: 50},
                 {name: '黔东南苗族侗族自治州',value: 50},
                 {name: '毕节地区',value: 50},
                 {name: '黔南布依族苗族自治州',value: 50},
                 {name: '铜仁地区',value: 50},
                 {name: '黔西南布依族苗族自治州',value: 50},
                 {name: '六盘水市',value: 50},
                 {name: '安顺市',value: 50},
                 {name: '贵阳市',value: 50},
                 {name: '烟台市',value: 50},
                 {name: '临沂市',value: 50},
                 {name: '潍坊市',value: 50},
                 {name: '青岛市',value: 50},
                 {name: '菏泽市',value: 50},
                 {name: '济宁市',value: 50},
                 {name: '德州市',value: 50},
                 {name: '滨州市',value: 50},
                 {name: '聊城市',value: 50},
                 {name: '东营市',value: 50},
                 {name: '济南市',value: 50},
                 {name: '泰安市',value: 50},
                 {name: '威海市',value: 50},
                 {name: '日照市',value: 50},
                 {name: '淄博市',value: 50},
                 {name: '枣庄市',value: 50},
                 {name: '莱芜市',value: 50},
                 {name: '赣州市',value: 50},
                 {name: '吉安市',value: 50},
                 {name: '上饶市',value: 50},
                 {name: '九江市',value: 50},
                 {name: '抚州市',value: 50},
                 {name: '宜春市',value: 50},
                 {name: '南昌市',value: 50},
                 {name: '景德镇市',value: 50},
                 {name: '萍乡市',value: 50},
                 {name: '鹰潭市',value: 50},
                 {name: '新余市',value: 50},
                 {name: '南阳市',value: 50},
                 {name: '信阳市',value: 50},
                 {name: '洛阳市',value: 50},
                 {name: '驻马店市',value: 50},
                 {name: '周口市',value: 50},
                 {name: '商丘市',value: 50},
                 {name: '三门峡市',value: 50},
                 {name: '新乡市',value: 50},
                 {name: '平顶山市',value: 50},
                 {name: '郑州市',value: 50},
                 {name: '安阳市',value: 50},
                 {name: '开封市',value: 50},
                 {name: '焦作市',value: 50},
                 {name: '许昌市',value: 50},
                 {name: '濮阳市',value: 50},
                 {name: '漯河市',value: 50},
                 {name: '鹤壁市',value: 50},
                 {name: '大连市',value: 50},
                 {name: '朝阳市',value: 50},
                 {name: '丹东市',value: 50},
                 {name: '铁岭市',value: 50},
                 {name: '沈阳市',value: 50},
                 {name: '抚顺市',value: 50},
                 {name: '葫芦岛市',value: 50},
                 {name: '阜新市',value: 50},
                 {name: '锦州市',value: 50},
                 {name: '鞍山市',value: 50},
                 {name: '本溪市',value: 50},
                 {name: '营口市',value: 50},
                 {name: '辽阳市',value: 50},
                 {name: '盘锦市',value: 50},
                 {name: '忻州市',value: 50},
                 {name: '吕梁市',value: 50},
                 {name: '临汾市',value: 50},
                 {name: '晋中市',value: 50},
                 {name: '运城市',value: 50},
                 {name: '大同市',value: 50},
                 {name: '长治市',value: 50},
                 {name: '朔州市',value: 50},
                 {name: '晋城市',value: 50},
                 {name: '太原市',value: 50},
                 {name: '阳泉市',value: 50},
                 {name: '六安市',value: 50},
                 {name: '安庆市',value: 50},
                 {name: '滁州市',value: 50},
                 {name: '宣城市',value: 50},
                 {name: '阜阳市',value: 50},
                 {name: '宿州市',value: 50},
                 {name: '黄山市',value: 50},
                 {name: '巢湖市',value: 50},
                 {name: '亳州市',value: 50},
                 {name: '池州市',value: 50},
                 {name: '合肥市',value: 50},
                 {name: '蚌埠市',value: 50},
                 {name: '芜湖市',value: 50},
                 {name: '淮北市',value: 50},
                 {name: '淮南市',value: 50},
                 {name: '马鞍山市',value: 50},
                 {name: '铜陵市',value: 50},
                 {name: '南平市',value: 50},
                 {name: '三明市',value: 50},
                 {name: '龙岩市',value: 50},
                 {name: '宁德市',value: 50},
                 {name: '福州市',value: 50},
                 {name: '漳州市',value: 50},
                 {name: '泉州市',value: 50},
                 {name: '莆田市',value: 50},
                 {name: '厦门市',value: 50},
                 {name: '丽水市',value: 50},
                 {name: '杭州市',value: 50},
                 {name: '温州市',value: 50},
                 {name: '宁波市',value: 50},
                 {name: '舟山市',value: 50},
                 {name: '台州市',value: 50},
                 {name: '金华市',value: 50},
                 {name: '衢州市',value: 50},
                 {name: '绍兴市',value: 50},
                 {name: '嘉兴市',value: 50},
                 {name: '湖州市',value: 50},
                 {name: '盐城市',value: 50},
                 {name: '徐州市',value: 50},
                 {name: '南通市',value: 50},
                 {name: '淮安市',value: 50},
                 {name: '苏州市',value: 50},
                 {name: '宿迁市',value: 50},
                 {name: '连云港市',value: 50},
                 {name: '扬州市',value: 50},
                 {name: '南京市',value: 50},
                 {name: '泰州市',value: 50},
                 {name: '无锡市',value: 50},
                 {name: '常州市',value: 50},
                 {name: '镇江市',value: 50},
                 {name: '吴忠市',value: 50},
                 {name: '中卫市',value: 50},
                 {name: '固原市',value: 50},
                 {name: '银川市',value: 50},
                 {name: '石嘴山市',value: 50},
                 {name: '儋州市',value: 50},
                 {name: '文昌市',value: 50},
                 {name: '乐东黎族自治县',value: 50},
                 {name: '三亚市',value: 50},
                 {name: '琼中黎族苗族自治县',value: 50},
                 {name: '东方市',value: 50},
                 {name: '海口市',value: 50},
                 {name: '万宁市',value: 50},
                 {name: '澄迈县',value: 50},
                 {name: '白沙黎族自治县',value: 50},
                 {name: '琼海市',value: 50},
                 {name: '昌江黎族自治县',value: 50},
                 {name: '临高县',value: 50},
                 {name: '陵水黎族自治县',value: 50},
                 {name: '屯昌县',value: 50},
                 {name: '定安县',value: 50},
                 {name: '保亭黎族苗族自治县',value: 50},
                 {name: '五指山市',value: 50}
             ];
			 
			 
			//配置信息：这六个分别代表了配置信息，从上到下分别为
			//图像名称，单位，图例数组，数据图例书名，值域最大值，值域最小值
			//但是edit页面做5个input就可以了，图例这里可以共用
			var reg = new RegExp('"',"g");
			var ex_text="默认名称";
			var ex_subtext="默认单位";
			var ex_legend=["图例"];
			var ex_series="图例";
			var ex_min="0";
			var ex_max="5000";
			
		function my(){
			var json=JSON.parse(jsondata);
		    //console.log(json);
		    var json_option=JSON.parse(jsonoption);
		    console.log(json_option);
		    for(var i=0;i<json.length;i++)
		    {
		    	ex[i].value=json[i].value;
		    	console.log(json[i].value);
		    }

		    //这里的问题就是要把双印号都干掉！！！
			ex_text=json_option[0].value;
			ex_subtext="单位："+json_option[1].value;
			
			//这里的逻辑是先把json对象string化，再去掉string化后的双引号
			ex_series=JSON.stringify(json_option[2].value);
			ex_series=ex_series.replace(reg, "")
			
			ex_legend[0]=ex_series;
			
			ex_min=JSON.stringify(json_option[3].value);
			ex_min=ex_min.replace(reg, "");
			
			ex_max=JSON.stringify(json_option[4].value);
			ex_max=ex_max.replace(reg, "");
	
			//console.log(typeof(ex_max));  
			
			
			// 路径配置
		    require.config({
		        paths: {
		            echarts: 'http://echarts.baidu.com/build/dist'
		        }
		    });

		    // 使用
		    require(
		            [
		                'echarts',
		                'echarts/chart/bar', // 使用柱状图就加载bar模块，按需加载
		                'echarts/chart/line',
		                'echarts/chart/pie',
		                'echarts/chart/map',
		                'echarts/chart/gauge',
		            ],
		            function (ec) {
		                // 基于准备好的dom，初始化echarts图表
		                var myChart = ec.init(document.getElementById('main'));

		                var option = {
		                   title : {
		                          text: ex_text,
		                          subtext: ex_subtext
		                          },
		                    tooltip : {
		                        trigger: 'item'
		                    },
		                    toolbox: {
		                        show : true,
		                        orient: 'vertical',
		                        x:'right',
		                        y:'center',
		                        feature : {
		                            mark : {show: true},
		                            dataView : {show: true, readOnly: false}
		                        }
		                    },
		                    series : [
		                        {
		                            tooltip: {
		                                trigger: 'item',
		                                formatter: '{b}'
		                            },
		                            name: '选择器',
		                            type: 'map',
		                            mapType: 'china',
		                            mapLocation: {
		                                x: 'left',
		                                y: 'top',
		                                width: '30%'
		                            },
		                            roam: true,
		                            selectedMode : 'single',
		                            itemStyle:{
		                                //normal:{label:{show:true}},
		                                emphasis:{label:{show:true}}
		                            },
		                            data:[
		                                {name: '北京', selected:false},
		                                {name: '天津', selected:false},
		                                {name: '上海', selected:false},
		                                {name: '重庆', selected:false},
		                                {name: '河北', selected:false},
		                                {name: '河南', selected:false},
		                                {name: '云南', selected:false},
		                                {name: '辽宁', selected:false},
		                                {name: '黑龙江', selected:false},
		                                {name: '湖南', selected:false},
		                                {name: '安徽', selected:false},
		                                {name: '山东', selected:false},
		                                {name: '新疆', selected:false},
		                                {name: '江苏', selected:false},
		                                {name: '浙江', selected:false},
		                                {name: '江西', selected:false},
		                                {name: '湖北', selected:false},
		                                {name: '广西', selected:false},
		                                {name: '甘肃', selected:false},
		                                {name: '山西', selected:false},
		                                {name: '内蒙古', selected:false},
		                                {name: '陕西', selected:false},
		                                {name: '吉林', selected:false},
		                                {name: '福建', selected:false},
		                                {name: '贵州', selected:false},
		                                {name: '广东', selected:false},
		                                {name: '青海', selected:false},
		                                {name: '西藏', selected:false},
		                                {name: '四川', selected:false},
		                                {name: '宁夏', selected:false},
		                                {name: '海南', selected:false},
		                                {name: '台湾', selected:false},
		                                {name: '香港', selected:false},
		                                {name: '澳门', selected:false}
		                            ]
		                        }
		                    ],
		                    animation: false
		                };
		                var ecConfig = require('echarts/config');
		                myChart.on(ecConfig.EVENT.MAP_SELECTED, function (param){
		                    var selected = param.selected;
		                    var selectedProvince;
		                    var name;
		                    for (var i = 0, l = option.series[0].data.length; i < l; i++) {
		                        name = option.series[0].data[i].name;
		                        option.series[0].data[i].selected = selected[name];
		                        if (selected[name]) {
		                            selectedProvince = name;
		                        }
		                    }
		                    if (typeof selectedProvince == 'undefined') {
		                        option.series.splice(1);
		                        option.legend = null;
		                        option.dataRange = null;
		                        myChart.setOption(option, true);
		                        return;
		                    }
		                    option.series[1] = {
		                        name: ex_series,
		                        type: 'map',
		                        mapType: selectedProvince,
		                        itemStyle:{
		                            normal:{label:{show:true}},
		                            emphasis:{label:{show:true}}
		                        },
		                        mapLocation: {
		                            x: '35%'
		                        },
		                        roam: true,
		                        data:ex,
		                    };
		                    option.legend = {
		                        x:'right',
		                        data:ex_legend,
		                    };
		                    option.dataRange = {
		                        orient: 'horizontal',
		                        x: 'right',
		                        min: ex_min,
		                        max: ex_max,
		                        color:['orange','yellow'],
		                        text:['高','低'],           // 文本，默认为数值文本
		                        splitNumber:0
		                    };
		                    myChart.setOption(option, true);
		                })

		                // 为echarts对象加载数据
		                        myChart.setOption(option);
		            }
		    );
		   
		}
	</script>
  <!-- content end -->
		
<footer>
  <hr>
  <p class="am-padding-left">© Powered by 汉景源. <a href="http://blog.csdn.net/lucahan" target="_blank">我的个人博客</a></p>
</footer>

<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/web/assets/js/jquery1.11.1.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/modernizr.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/rem.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/respond.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.legacy.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.min.js"></script>
<!--<![endif]-->
<script src="${pageContext.request.contextPath}/web/assets/js/app.js"></script>
</body>
</html>
