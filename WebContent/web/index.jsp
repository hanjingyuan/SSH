<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>OnlineData</title>
  <meta name="description" content="">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/web/assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/web/assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/admin.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<header class="am-topbar admin-header">
  <div class="am-topbar-brand">
    <strong>OnlineData</strong> <small>在线制图网站</small>
  </div>

  <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>
	<!-- 上方标签 -->
	<%@ include file="menuTop.jsp" %>
	<!-- 上方标签 -->
</header>

<div class="am-cf admin-main">
  <!-- sidebar start -->
  <div class="admin-sidebar">
	
	<!-- 左侧导航开始 -->
	<%@ include file="/web/menuLeft.jsp" %>
	<!-- 左侧导航结束-->
    
    <!-- 左下侧广播 -->
    <%@ include file="/web/menuAd.jsp" %>
    <!-- 左下侧广播 -->
  
  <!-- sidebar end -->
<!-- content start -->
  <div class="admin-content" >
	 <div class="admin-content-body">
      <div class="am-cf am-padding am-padding-bottom-0">
        <div class="am-fl am-cf">
          <strong class="am-text-primary am-text-lg">首页</strong> /
          <small>Index</small>
        </div>
      </div>
      <hr/>
      <div class="am-g">
        <div class="am-u-sm-12 am-u-sm-centered">
	          <div id="main" style="height:600px">
	          	  <img src="${pageContext.request.contextPath}/web/assets/pic2.png" style="height:400px;right:50%">
		          <h2>欢迎来到OnlineData在线制图系统</h2>
		          <p>本系统为共享杯比赛“分省统计数据在线制图工具”题目作品。</p>
		          <p>项目整体采用JAVA语言编写，后端使用Struts2，Spring，Hibernate框架，前端使用了开源的Echarts来完成统计图的制作。</p>
		          <p>前端采用了开源的Echarts框架，完成制作统计图的需求，制作统计图时，快速，方便，制作的统计图精美，实用、</p>
		          <p>网站左侧可以查看DEMO示例，详细的帮助文档，以及制作统计图。</p>
		          
	          </div>
        </div>
      </div>
      </div>
  </div>
  <!-- content end -->
		


<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/web/assets/js/jquery1.11.1.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/modernizr.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/rem.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/respond.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.legacy.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="${pageContext.request.contextPath}/web/assets/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.min.js"></script>
<!--<![endif]-->
<script src="${pageContext.request.contextPath}/web/assets/js/app.js"></script>
</body>
</html>
