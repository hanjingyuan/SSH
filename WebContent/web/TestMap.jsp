<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body onload="my()">
	<div id="main" style="height:650px"></div>
	<p><s:property value="jsondata"/></p>
	<button value="test" onclick="my()"></button>
	<script src="http://echarts.baidu.com/build/dist/echarts.js"></script>
	<script type="text/javascript">
		var s="<s:property value="jsondata"/>";
		 s=s.replace(/&quot;/g,'"');
		 var ex=[
				    {"name": "北京", "value": "23.1"},
				    {"name": "天津", "value": "9.2"},
				    {"name": "河北", "value": "9.2"},
				    {"name": "山西", "value": "9.2"},
				    {"name": "内蒙古", "value": "9.2"},
				    {"name": "辽宁", "value": "9.2"},
				    {"name": "吉林", "value": "9.2"},
				    {"name": "黑龙江", "value": "9.2"},
				    {"name": "上海", "value": "9.2"},
				    {"name": "江苏", "value": "9.2"},
				    {"name": "浙江", "value": "9.2"},
				    {"name": "安徽", "value": "9.2"},
				    {"name": "福建", "value": "9.2"},
				    {"name": "江西", "value": "9.2"},
				    {"name": "山东", "value": "9.2"},
				    {"name": "河南", "value": "9.2"},
				    {"name": "湖北", "value": "9.2"},
				    {"name": "湖南", "value": "9.2"},
				    {"name": "广东", "value": "9.2"},
				    {"name": "广西", "value": "9.2"},
				    {"name": "海南", "value": "9.2"},
				    {"name": "重庆", "value": "9.2"},
				    {"name": "四川", "value": "9.2"},
				    {"name": "贵州", "value": "9.2"},
				    {"name": "云南", "value": "9.2"},
				    {"name": "西藏", "value": "9.2"},
				    {"name": "陕西", "value": "9.2"},
				    {"name": "甘肃", "value": "9.2"},
				    {"name": "青海", "value": "9.2"},
				    {"name": "宁夏", "value": "9.2"},
				    {"name": "新疆", "value": "9.2"}
				]
		function my(){
			var json=JSON.parse(s);
		    console.log(ex);
		    for(var i=0;i<json.length;i++)
		    {
		    	ex[i].value=json[i].value;
		    }
			
		    // 路径配置
		    require.config({
		        paths: {
		            echarts: 'http://echarts.baidu.com/build/dist'
		        }
		    });

		    // 使用
		    require(
		            [
		                'echarts',
		                'echarts/chart/bar', // 使用柱状图就加载bar模块，按需加载
		                'echarts/chart/line',
		                'echarts/chart/pie',
		                'echarts/chart/map',
		                'echarts/chart/gauge',
		            ],
		            function (ec) {
		                // 基于准备好的dom，初始化echarts图表
		                var myChart = ec.init(document.getElementById('main'));

		                var option = {
		                    title : {
		                        text: '全国水资源情况',
		                        subtext: '2010年',
		                        x:'center'
		                    },
		                    tooltip : {
		                        trigger: 'item',
		                        formatter: "{a} <br/>{b}  :{c}亿立方米 "
		                    },
		                    legend: {
		                        orient: 'vertical',
		                        x:'left',
		                        data:['水资源总量']
		                    },
		                    dataRange: {
		                        min: 0,
		                        max: 5000,
		                        x: 'left',
		                        y: 'bottom',
		                        text:['高','低'],           // 文本，默认为数值文本
		                        calculable : true
		                    },
		                    toolbox: {
		                        show: true,
		                        orient : 'vertical',
		                        x: 'right',
		                        y: 'center',
		                        feature : {
		                            mark : {show: true},
		                            dataView : {show: true, readOnly: false},
		                            restore : {show: true},
		                            saveAsImage : {show: true}
		                        }
		                    },
		                    roamController: {
		                        show: true,
		                        x: 'right',
		                        mapTypeControl: {
		                            'china': true
		                        }
		                    },
		                    series : [
		                        {
		                            name: '水资源总量',
		                            type: 'map',
		                            mapType: 'china',
		                            roam: false,
		                            itemStyle:{
		                                normal:{label:{show:true}},
		                                emphasis:{label:{show:true}}
		                            },
		                            data:ex
		                        }
		                                            
		                    ]
		                };

		                // 为echarts对象加载数据
		                myChart.setOption(option);
		            }
		    );
			
		}
	</script>
</body>
</html>