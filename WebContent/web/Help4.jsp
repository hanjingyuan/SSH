<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>OnlineData</title>
  <meta name="description" content="">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/web/assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/web/assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/admin.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<header class="am-topbar admin-header">
  <div class="am-topbar-brand">
    <strong>OnlineData</strong> <small>在线制图网站</small>
  </div>

  <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>
	<!-- 上方标签 -->
	<%@ include file="menuTop.jsp" %>
	<!-- 上方标签 -->
</header>

<div class="am-cf admin-main">
  <!-- sidebar start -->
  <div class="admin-sidebar">
	
	<!-- 左侧导航开始 -->
	<%@ include file="/web/menuLeft.jsp" %>
	<!-- 左侧导航结束-->
    
    <!-- 左下侧广播 -->
    <%@ include file="/web/menuAd.jsp" %>
    <!-- 左下侧广播 -->
  
  <!-- sidebar end -->
<!-- content start -->
  <div class="admin-content" >
	 <div class="admin-content-body">
      <div class="am-cf am-padding am-padding-bottom-0">
        <div class="am-fl am-cf">
          <strong class="am-text-primary am-text-lg">首页</strong> /
          <small>Index</small>
        </div>
      </div>
      <hr/>
      <div class="am-g">
        <div class="am-u-sm-12 am-u-sm-centered">
	          <div id="main" >
	          	 <h1>四、	制图模块使用帮助</h1>
	          	 <p>地图制图模块有2种方式供用户来使用。一种是用户可以下载Excel模板，填写相应数据，然后重新上传Excel文件就可以制图。第二种方式就是，用户可以通过在线的方式，填写数据，然后保存数据，制图即可。</p>
	          	 <p>除了两个数据的模块以外，用户还需要填写统计图工具箱配置，在这里面，可能会需要用户填写统计图名称，单位，图例名称，数据值域的范围等等。通过填写选项设置，可以完善统计图，使得用户制作的统计图更加的规范。</p>
	          	 <p>接下来，按照不同的统计图，依次介绍如何进行统计图的制作。</p>
	          	 
	          	 <hr>
	          	 <h2>1.	地图统计图</h2>
	          	 <p>（1）	填写统计图工具箱配置。进入地图统计图制图页面之后，用户首先需要填写统计图的统计图工具箱配置。在地图统计图中，需要填写的是统计图名称，数据单位，图例名称，值域最小值和值域最大值，填写完成之后只需要填写保存数据即可。</p>
	          	 <p>例如，用户制作的是2010年中国人口统计图，单位是万人。那么需要填写统计图名称为“2010年中国人口统计图”，数据单位为“万人”，图例名称可填写为“人口数量”，值域的最小值可填为0，值域的最大值可以根据最多人口省份的数量进行填写。</p>
	          	 <p>（2）	使用上传模式制图。用户完成统计图工具箱配置后，可以填写相应的数据了。用户可以直接点击右上角的下载模板按钮，将模板文件下载到本地，更改Excel模板中的内容。填写完成之后，点击选择文件，选中刚才更改好的文件，然后点击保存即可完成制图。</p>
	          	 <p>（3）	如果用户认为不需要使用上传模式制图，那么用户可以在制图页面中，对表格中的内容数据进行补充，填写完成之后，只需要先点击保存数据，确认无误之后，点击提交数据就可以完成制图了。</p>
	          	
	          	
	          	<hr>
	          	<h2>2.	地图饼图统计图</h2>
	          	<p>地图饼图统计图的制图方法与地图统计图的制图方法相同。</p>
	          	<p></p>
	          	<p></p>
	          	
	          		          	
	          	<hr>
	          	<h2>3.地图柱状图统计图</h2>
	          	<p>	地图柱状图统计图的制图方法与地图统计图的制图方法相同。</p>
	          	<p></p>
	          	<p></p>
	          	
	          		          	
	          	<hr>
	          	<h2>4.地图下钻统计图</h2>
	          	<p>地钻下钻统计图的制图方法与前面的制图方法基本相同。	</p>
	          	<p>但是要注意的是，由于地图下钻图中的数据量较多，因为不再提供在线输入数据制图的方法。用户如果想制作地图下钻统计图，就必须要下载模板，然后使用Excel对模板的数据进行编辑，然后重新上传模板中的数据。这样就可以完成制图了。</p>
	          	<p></p>
	          	
	          		          	
	          	<hr>
	          	<h2>5.折线图统计图</h2>
	          	<p>折线图中的制图方法也仍与前面基本相同。</p>
	          	<p>但是由于折线统计图中，x轴的数量不一定是确定的，所以不提供在线输入的模式。用户如果想制作折线图统计图，就必须要下载模板，然后使用Excel对模板的数据进行编辑，然后重新上传模板中的数据。这样就可以完成制图了。</p>
	          	<p></p>
	          	
	          		          	
	          	<hr>
	          	<h2>6.	柱状图统计图</h2>
	          	<p>柱状图统计图的制图方法与地图统计图的制图方法相同。</p>
	          	<p></p>
	          	<p></p>
	          	
	          		          	
	          	<hr>
	          	<h2>7.	饼状图统计图</h2>
	          	<p>饼状图统计图的制图方法与地图统计图的制图方法相同。</p>
	          	<p></p>
	          	<p></p>
	          	
	          		          	
	          	<hr>
	          	<h2>8.	饼状图折线图统计图</h2>
	          	<p>折线图中的制图方法也仍与前面基本相同。</p>
	          	<p>但是由于折线统计图中，有两列数据，所以不提供在线输入的模式。用户如果想制作折线图统计图，就必须要在线填写数据，然后保存数据。这样就可以完成制图了。</p>
	          	<p></p>
	          	
	          		          	
	          	<hr>
	          	<h2></h2>
	          	<p></p>
	          	<p></p>
	          	<p></p>
	          
	          
	          
	          
	          
	          </div>
        </div>
      </div>
      </div>
  </div>
  <!-- content end -->
		


<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/web/assets/js/jquery1.11.1.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/modernizr.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/rem.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/respond.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.legacy.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="${pageContext.request.contextPath}/web/assets/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.min.js"></script>
<!--<![endif]-->
<script src="${pageContext.request.contextPath}/web/assets/js/app.js"></script>
</body>
</html>
