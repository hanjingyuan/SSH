<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>OnlineData</title>
  <meta name="description" content="">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/web/assets/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/web/assets/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/amazeui.min.css"/>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/web/assets/css/admin.css">
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<header class="am-topbar admin-header">
  <div class="am-topbar-brand">
    <strong>OnlineData</strong> <small>在线制图管理系统</small>
  </div>

  <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>
	<!-- 上方标签 -->
	<%@ include file="menuTop.jsp" %>
	<!-- 上方标签 -->
</header>

<div class="am-cf admin-main">
  <!-- sidebar start -->
  <div class="admin-sidebar">
	
	<!-- 左侧导航开始 -->
	<%@ include file="/web/menuLeft.jsp" %>
	<!-- 左侧导航结束-->
    
    <!-- 左下侧广播 -->
    <%@ include file="/web/menuAd.jsp" %>
    <!-- 左下侧广播 -->
  
  <!-- sidebar end -->
<!-- content start -->
  				<div class="am-cf admin-main">
  <!-- sidebar start -->
  <div class="admin-sidebar am-offcanvas" id="admin-offcanvas">
    <div class="am-offcanvas-bar admin-offcanvas-bar">
      <ul class="am-list admin-sidebar-list">
        <li><a href="admin-index.html"><span class="am-icon-home"></span> 首页</a></li>
        <li class="admin-parent">
          <a class="am-cf" data-am-collapse="{target: '#collapse-nav'}"><span class="am-icon-file"></span> 页面模块 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
          <ul class="am-list am-collapse admin-sidebar-sub am-in" id="collapse-nav">
            <li><a href="admin-user.html" class="am-cf"><span class="am-icon-check"></span> 个人资料<span class="am-icon-star am-fr am-margin-right admin-icon-yellow"></span></a></li>
            <li><a href="admin-help.html"><span class="am-icon-puzzle-piece"></span> 帮助页</a></li>
            <li><a href="admin-gallery.html"><span class="am-icon-th"></span> 相册页面<span class="am-badge am-badge-secondary am-margin-right am-fr">24</span></a></li>
            <li><a href="admin-log.html"><span class="am-icon-calendar"></span> 系统日志</a></li>
            <li><a href="admin-404.html"><span class="am-icon-bug"></span> 404</a></li>
          </ul>
        </li>
        <li><a href="admin-table.html"><span class="am-icon-table"></span> 表格</a></li>
        <li><a href="admin-form.html"><span class="am-icon-pencil-square-o"></span> 表单</a></li>
        <li><a href="#"><span class="am-icon-sign-out"></span> 注销</a></li>
      </ul>
      <div class="am-panel am-panel-default admin-sidebar-panel">
        <div class="am-panel-bd">
          <p><span class="am-icon-bookmark"></span> 公告</p>
          <p>时光静好，与君语；细水流年，与君同。—— Amaze UI</p>
        </div>
      </div>
      <div class="am-panel am-panel-default admin-sidebar-panel">
        <div class="am-panel-bd">
          <p><span class="am-icon-tag"></span> wiki</p>
          <p>Welcome to the Amaze UI wiki!</p>
        </div>
      </div>
    </div>
  </div>
  <!-- sidebar end -->
  <!-- content start -->
  <div class="admin-content">
    <div class="admin-content-body">
      <div class="am-cf am-padding am-padding-bottom-0">
        <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">地图统计图制作</strong> / <small>Map Edit</small></div>
      </div>
      <hr/>
      <div class="am-g">
        <div class="am-u-sm-12 am-u-md-4 am-u-md-push-8">
          <div class="am-panel am-panel-default">
            <div class="am-panel-bd">
              <div class="am-g">
                <div class="am-u-md-4">
                  <img class="am-img-circle am-img-thumbnail" src="${pageContext.request.contextPath}/web/assets/excel2013icon.png" alt=""/>
                </div>
                <div class="am-u-md-8">
                  <p>您可以使用本地的xls文件上传制作统计图表</p>
                  <form class="am-form" action="${pageContext.request.contextPath}/data_uploadmap.action" method="post" enctype="multipart/form-data">
                    <div class="am-form-group">
                      <input type="file" id="user-pic" name="excel">
                      <p class="am-form-help">请选择要上传的文件...</p>
                      <input type="hidden" class="jsonoption" name="jsonoption">
                      <button type="submit" class="am-btn am-btn-success" >保存</button>
                    </div>
                  </form>
                  	<form action="${pageContext.request.contextPath}/data_downMap.action">
						<input type="submit" class="am-btn am-btn-primary am-btn-xs"  value="下载模板"> 
					</form>
                </div>
              </div>
            </div>
          </div>
          <div class="am-panel am-panel-default">
            <div class="am-panel-bd">
              <div class="user-info">
                <strong><p class=".am-sans-serif">统计图工具箱配置：</p></strong> <br>  
                <form id="option">
                <input type="text" class="am-form-field am-round" name="ex_text" placeholder="请填写统计图名称"/><br>  
                <input type="text" class="am-form-field am-round" name="ex_subtext" placeholder="请填写数据单位"/> <br>  
                <input type="text" class="am-form-field am-round" name="ex_legend" placeholder="请填写图例名称"/><br>  
                <input type="text" class="am-form-field am-round" name="ex_min" placeholder="请填写值域最小值"/><br>  
                <input type="text" class="am-form-field am-round" name="ex_max" placeholder="请填写值域最大值"/><br>  
                <button type="button" onclick="submitoption()" id="optionbutton" class="am-btn am-btn-success am-round">保存数据</button>
                </form>    		             	
              </div>
            </div>
          </div>
        </div>				
		<!-- 表单区域 -->
        <div class="am-u-sm-12 am-u-md-8 am-u-md-pull-4">
          	<form action="" id="formdata">
          	<table class="am-table am-table-bordered am-table-striped am-table-compact">
				  <thead>
				  <tr>
				    <th>省份名称</th>
				    <th>填入数据</th>
				  </tr>
				  </thead>
				  <tbody>
					<tr>
			        	<td>1.北京</td>
			            <td><input type="text" name="bejing1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>2.天津</td>
			            <td><input type="text" name="tianjin1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>3.河北</td>
			            <td><input type="text" name="hebei1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>4.上海</td>
			            <td><input type="text" name="shanxi1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>5.内蒙古</td>
			            <td><input type="text" name="neimenggu1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>6.辽宁</td>
			            <td><input type="text" name="liaoning1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>7.吉林</td>
			            <td><input type="text" name="jilin1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>8.黑龙江</td>
			            <td><input type="text" name="heilongjiang1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>9.上海</td>
			            <td><input type="text" name="shanghai1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>10.江苏</td>
			            <td><input type="text" name="jiangsu1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>11.浙江</td>
			            <td><input type="text" name="zhejiang1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>12.安徽</td>
			            <td><input type="text" name="anhui1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>13.福建</td>
			            <td><input type="text" name="fujian1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>14.江西</td>
			            <td><input type="text" name="jiangxi1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>15.山东</td>
			            <td><input type="text" name="shandong1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>16.河南</td>
			            <td><input type="text" name="henan1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>17.湖北</td>
			            <td><input type="text" name="hubei1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>18.湖南</td>
			            <td><input type="text" name="hunan1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>19.广东</td>
			            <td><input type="text" name="guangdong1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>20.广西</td>
			            <td><input type="text" name="guangxi1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>21.海南</td>
			            <td><input type="text" name="hainan1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>22.重庆</td>
			            <td><input type="text" name="chongqing1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>23.四川</td>
			            <td><input type="text" name="sichuang1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>24.贵州</td>
			            <td><input type="text" name="guizhou1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>25.云南</td>
			            <td><input type="text" name="yunnan1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>26.西藏</td>
			            <td><input type="text" name="xizang1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>27.陕西</td>
			            <td><input type="text" name="shanxi1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>28.甘肃</td>
			            <td><input type="text" name="gansu1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>29.青海</td>
			            <td><input type="text" name="qinghai1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>30.宁夏</td>
			            <td><input type="text" name="ningxia1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>31.新疆</td>
			            <td><input type="text" name="xinjiang1" value="1000" ></td>
			        </tr>
			        <tr>
			        	<td>32.香港</td>
			            <td><input type="text" name="xianggang1" value="1000" ></td>
			        </tr>			        
			        <tr>
			        	<td>33.澳门</td>
			            <td><input type="text" name="aomen1" value="1000" ></td>
			        </tr>		                
			        <tr>
			        	<td>34.台湾</td>
			            <td><input type="text" name="taiwan1" value="1000" ></td>
			        </tr>					
			  </tbody>
			</table>
				<input type="button"  class="am-btn am-btn-danger" onclick="changejson()" value="保存数据" >
			</form>
			<form action="${pageContext.request.contextPath}/data_Map.action" method="post">
				<input type="hidden" id="jsondata" name="jsondata" >
				<input type="hidden" class="jsonoption" name="jsonoption" >
				<input type="submit"  class="am-btn am-btn-success" value="提交数据" >
			</form>
        </div>         
      </div>
    </div>
  </div>
  <!-- content end -->
		<script type="text/javascript">
			//把通过input输入的分省数据转换成json
	  		function changejson(){
	  			 var jsondata=JSON.stringify($("#formdata").serializeArray());
	  			 document.getElementById("jsondata").value=jsondata;
	  			 console.log(jsondata);
	  			 alert("保存数据成功，请提交数据");
	  		}
	  		//把通过input输入的option数据转换成json
	  		function submitoption(){
	  			var jsonoption=JSON.stringify($("#option").serializeArray());
	  			$(".jsonoption").val(jsonoption) ;
	  			console.log(jsonoption);
	  			console.log($(".jsonoption").val());
	  			alert("保存配置数据成功");
	  		}
        </script>
<footer>
  <hr>
  <p class="am-padding-left">© Powered by 汉景源. <a href="http://blog.csdn.net/lucahan" target="_blank">我的个人博客</a></p>
</footer>

<!--[if lt IE 9]>
<script src="${pageContext.request.contextPath}/web/assets/js/jquery1.11.1.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/modernizr.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/rem.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/polyfill/respond.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.legacy.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="${pageContext.request.contextPath}/web/assets/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/web/assets/js/amazeui.min.js"></script>
<!--<![endif]-->
<script src="${pageContext.request.contextPath}/web/assets/js/app.js"></script>
</body>
</html>
