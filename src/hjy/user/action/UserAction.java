package hjy.user.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import hjy.user.service.UserService;
import hjy.user.vo.User;

public class UserAction extends ActionSupport implements ModelDriven<User> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 0.模型驱动的方法
	 * 1.设置好vo之后，要进行模型驱动，设置ModelDriven<User>
	 * 2.然后新建new 一个 user
	 * 3.然后提供一个User getModel()的方法
	 * 4.将get方法的返回值改为user
	 */
	
	/**
	 * 0.service和dao方法都要配置到applicationcontext中
	 * 1.action也要配置
	 */
	private User user =new User();
	
	public User getModel(){
		return user;
	}
	
	private UserService userService=new UserService();
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	private File myFile; 
	private String myFileContentType; 
	private String myFileFileName; 
	private String destPath; 
	
	
	
	
	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}

	public String getDestPath() {
		return destPath;
	}

	public void setDestPath(String destPath) {
		this.destPath = destPath;
	}

	/**
	 * ajax异步校验
	 * @return
	 * @throws IOException 
	 */
	public String findByName() throws IOException{
		//调用service来进行查询
		User exisUser=userService.findByName(user.getUsername());
		//获得response对象，向着页面输出
		HttpServletResponse response= ServletActionContext.getResponse();
		response.setContentType("text/html;charaset=UTF-8");
		response.setCharacterEncoding("UTF-8"); 
		if(exisUser!=null){
			//用户名存在
			response.getWriter().println("<font color='red'>不能用</font>");
		}else{
			//用户名不存在，可以使用
			response.getWriter().println("<font color='green'>能用</font>");
		}
		return NONE;
	}
	
	/**
	 * 注册方法
	 * @return
	 */
	public String regist(){
		userService.save(user);
		this.addActionMessage("注册成功!请登陆!");
		return "msg";
	}
	
	/**
	 * 登陆的方法
	 * @return
	 */
	public String login(){
		User existuser=userService.login(user);
		if (existuser==null) {
			//失败
			this.addActionError("登录失败，用户名或者密码错误");
			return LOGIN;
		} else {
			//成功
			//存入session
			//页面跳转
			ServletActionContext.getRequest().getSession().setAttribute("existuser", existuser);
			return "loginSuccess";
		}
	}
	
	/**
	 * 退出
	 * @return
	 */
	public String quit(){
		ServletActionContext.getRequest().getSession().invalidate();
		return "quit";
	}
	
	
	public String upload(){
		destPath = "E:/work/";
		try{ 
			System.out.println("Src File name: " + myFile); 
			System.out.println("Dst File name: " + myFileFileName); 
			File destFile  = new File(destPath, myFileFileName); 
			FileUtils.copyFile(myFile, destFile); }
		catch(IOException e){ 
			e.printStackTrace();
			return ERROR; 
		}
		this.addActionMessage("上传成功！");
		return "uploadSuccess";
	}
	
	


}
