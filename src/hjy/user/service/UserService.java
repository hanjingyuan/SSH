package hjy.user.service;

import org.springframework.transaction.annotation.Transactional;

import hjy.user.dao.UserDao;
import hjy.user.vo.User;

@Transactional
public class UserService {
	
	/**
	 * 1.注意业务层一定要配置@Transactional
	 * 2.要在service中注入dao
	 */
	private UserDao userDao;

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
	//查询用户名
	public User findByName(String username){
		return userDao.findByUserName(username);
	}
	
	//注册保存
	public void save(User user) {
		userDao.save(user);
		
	}
	
	//登陆
	public User login(User user) {
		// TODO Auto-generated method stub
		return userDao.login(user);
	}

}
