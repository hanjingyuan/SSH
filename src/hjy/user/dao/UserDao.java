package hjy.user.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import hjy.user.vo.User;

public class UserDao  extends HibernateDaoSupport{
	
	/**
	 * 0.dao层要继承HibernateDaoSupport
	 * 1.这样可以直接给我们提供模板
	 * 2.给DAO配置sessionfactory，可以直接配置dao模板
	 * 3.<property name="sessionFactory" ref="sessionFactory"/>
	 */
	/**
	 * 查找用户名是否为空，用户注册时用户名的判断
	 * @param username
	 * @return
	 */
	public User findByUserName(String username){
		String hql="from User where username = ?";
		List<User> list=this.getHibernateTemplate().find(hql,username);
		if(list != null&&list.size()>0){
			return list.get(0);
		}else{
			return null;
		}
		}
	
	//注册用户 存入数据库
	public void save(User user) {
		this.getHibernateTemplate().save(user);
		
	}

	public User login(User user) {
		String hql="from User where username=? and password=?";
		
		List<User> list =this.getHibernateTemplate().find(hql,user.getUsername(),user.getPassword());
		if(list!=null&&list.size()>0){
			return list.get(0);
		}else{
			return null;
		}
	}

}
