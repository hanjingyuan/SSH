package hjy.data.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class Excel {
	
	public String read(String fileurl) throws IOException{
		String url=fileurl;
		//读入流
		InputStream is = new FileInputStream(url);
		//写入文件
		HSSFWorkbook hssfWorkbook = new HSSFWorkbook(is);
		String name=null;
		String data=null;
		String json="";
		
		for (int numSheet = 0; numSheet < hssfWorkbook.getNumberOfSheets(); numSheet++) {
			//使用每一张sheet
			HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(numSheet);
			if (hssfSheet == null) {
				continue;
			}
			// 循环行Row
			for (int rowNum = 1; rowNum <= hssfSheet.getLastRowNum(); rowNum++) {
				HSSFRow hssfRow = hssfSheet.getRow(rowNum);
				if (hssfRow != null) {
					HSSFCell exname = hssfRow.getCell(0);
					name=getValue(exname);
					HSSFCell exdata = hssfRow.getCell(1);
					data=getValue(exdata);
					json+="{"+"\""+"name"+"\""+":"+"\""+name+"\""+","+"\""+"value"+"\""+":"+"\""+data+"\""+"}"+",";
				}
			}
		}
		json="["+json+"]";
		json=json.substring(0, json.length()-2);
		json=json+"]";
		return json;
	}
	
	 @SuppressWarnings("static-access")
		private String getValue(HSSFCell hssfCell) {
		        if (hssfCell.getCellType() == hssfCell.CELL_TYPE_BOOLEAN) {
		            // 返回布尔类型的值
		            return String.valueOf(hssfCell.getBooleanCellValue());
		        } else if (hssfCell.getCellType() == hssfCell.CELL_TYPE_NUMERIC) {
		            // 返回数值类型的值
		            return String.valueOf(hssfCell.getNumericCellValue());
		        } else {
		            // 返回字符串类型的值
		            return String.valueOf(hssfCell.getStringCellValue());
		        }
		    }

}
