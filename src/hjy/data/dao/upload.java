package hjy.data.dao;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class upload {
	
	public void uploadfile(String destPath,File excel,String excelFileName) throws IOException{
		String dest_path=destPath;
		File destFile  = new File(dest_path, excelFileName); 
		FileUtils.copyFile(excel, destFile); 
		
	}

}
