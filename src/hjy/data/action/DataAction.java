package hjy.data.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import hjy.data.dao.Excel;
import hjy.data.dao.upload;
import hjy.data.service.DataService;
import hjy.data.vo.Data;

public class DataAction extends ActionSupport implements ModelDriven<Data> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String destpath="C:/work/";
	
	//1.模型驱动
	private Data data=new Data();
	public Data getModel() {
		return data;
	}
	//2.注入service 
	private DataService dataService;
	public void setDataService(DataService dataService) {
		this.dataService = dataService;
	}
	//3.上传文件所用
	private upload upexcel=new upload();
	
	public upload getUpexcel() {
		return upexcel;
	}
	public void setUpexcel(upload upexcel) {
		this.upexcel = upexcel;
	}

	//4.通过poi读取excel
	private Excel excel=new Excel();
	
	public Excel getExcel() {
		return excel;
	}
	public void setExcel(Excel excel) {
		this.excel = excel;
	}
	
	//5.下载文件所用的到
	private String filename;
	private InputStream inputStream;
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	/**
	 * Map在线模式
	 * 1.通过模型驱动接收前台转换的json，2.把json放到valuestack传回去
	 * @return
	 */
	public String Map(){
		ServletActionContext.getRequest().getSession().setAttribute("jsonoption", data.getJsonoption());
		ServletActionContext.getRequest().getSession().setAttribute("jsondata", data.getJsondata());
		return "Map";
	}
	
	/**
	 * Map上传模式
	 * 1.上传excel，2.用poi读取excel，转成json，3.把json放进valuestack中，4.前台js转换出图
	 * @return
	 * @throws IOException
	 */
	public String uploadmap() throws IOException{
		upexcel.uploadfile(destpath,data.getExcel(), data.getExcelFileName());
		String readpath=destpath+data.getExcelFileName();
		String jsondata=excel.read(readpath);
		ServletActionContext.getRequest().getSession().setAttribute("jsonoption", data.getJsonoption());
		ServletActionContext.getRequest().getSession().setAttribute("jsondata", jsondata);
		return "Map";
	}
	
	/**
	 * MapPie在线模式
	 * @return
	 */
	public String MapPie(){
		ServletActionContext.getRequest().getSession().setAttribute("jsonoption", data.getJsonoption());
		ServletActionContext.getRequest().getSession().setAttribute("jsondata", data.getJsondata());
		return "MapPie";
	}
	
	
	/**
	 * MapPie上传模式
	 * @return
	 * @throws IOException 
	 */
	public String uploadMapPie() throws IOException{
		upexcel.uploadfile(destpath,data.getExcel(), data.getExcelFileName());
		String readpath=destpath+data.getExcelFileName();
		String jsondata=excel.read(readpath);
		ServletActionContext.getRequest().getSession().setAttribute("jsonoption", data.getJsonoption());
		ServletActionContext.getRequest().getSession().setAttribute("jsondata", jsondata);
		return "MapPie";
	}
	
	/**
	 * MapBar在线模式
	 * @return
	 */
	public String MapBar(){
		ServletActionContext.getRequest().getSession().setAttribute("jsonoption", data.getJsonoption());
		ServletActionContext.getRequest().getSession().setAttribute("jsondata", data.getJsondata());
		return "MapBar";
	}
	
	
	/**
	 * MapBar上传模式
	 * @return
	 * @throws IOException 
	 */
	public String uploadMapBar() throws IOException{
		upexcel.uploadfile(destpath,data.getExcel(), data.getExcelFileName());
		String readpath=destpath+data.getExcelFileName();
		String jsondata=excel.read(readpath);
		ServletActionContext.getRequest().getSession().setAttribute("jsonoption", data.getJsonoption());
		ServletActionContext.getRequest().getSession().setAttribute("jsondata", jsondata);
		return "MapBar";
	}
	
	
	/**
	 * MapLev在线模式
	 * @return
	 */
	public String MapLev(){
		ServletActionContext.getRequest().getSession().setAttribute("jsonoption", data.getJsonoption());
		ServletActionContext.getRequest().getSession().setAttribute("jsondata", data.getJsondata());
		return "MapLev";
	}
	
	
	/**
	 * MapLev上传模式
	 * @return
	 * @throws IOException 
	 */
	public String uploadMapLev() throws IOException{
		upexcel.uploadfile(destpath,data.getExcel(), data.getExcelFileName());
		String readpath=destpath+data.getExcelFileName();
		String jsondata=excel.read(readpath);
		ServletActionContext.getRequest().getSession().setAttribute("jsonoption", data.getJsonoption());
		ServletActionContext.getRequest().getSession().setAttribute("jsondata", jsondata);
		return "MapLev";
	}
	
	/**
	 * MapLine上传模式
	 * @return
	 * @throws IOException 
	 */
	public String uploadLine() throws IOException{
		upexcel.uploadfile(destpath,data.getExcel(), data.getExcelFileName());
		String readpath=destpath+data.getExcelFileName();
		String jsondata=excel.read(readpath);
		ServletActionContext.getRequest().getSession().setAttribute("jsonoption", data.getJsonoption());
		ServletActionContext.getRequest().getSession().setAttribute("jsondata", jsondata);
		return "Line";
	}


	
	
	/**
	 * Bar在线模式
	 * @return
	 */
	public String Bar(){
		ServletActionContext.getRequest().getSession().setAttribute("jsonoption", data.getJsonoption());
		ServletActionContext.getRequest().getSession().setAttribute("jsondata", data.getJsondata());
		return "Bar";
	}
	
	
	/**
	 * Bar上传模式
	 * @return
	 * @throws IOException 
	 */
	public String uploadBar() throws IOException{
		upexcel.uploadfile(destpath,data.getExcel(), data.getExcelFileName());
		String readpath=destpath+data.getExcelFileName();
		String jsondata=excel.read(readpath);
		ServletActionContext.getRequest().getSession().setAttribute("jsonoption", data.getJsonoption());
		ServletActionContext.getRequest().getSession().setAttribute("jsondata", jsondata);
		return "Bar";
		}
	
	/**
	 * Pie上传模式
	 * @return
	 * @throws IOException 
	 */
	public String uploadPie() throws IOException{
		upexcel.uploadfile(destpath,data.getExcel(), data.getExcelFileName());
		String readpath=destpath+data.getExcelFileName();
		String jsondata=excel.read(readpath);
		ServletActionContext.getRequest().getSession().setAttribute("jsonoption", data.getJsonoption());
		ServletActionContext.getRequest().getSession().setAttribute("jsondata", jsondata);
		return "Pie";
		}
	
	
	
	/**
	 * BarLine在线模式
	 * @return
	 */
	public String BarLine(){
		ServletActionContext.getRequest().getSession().setAttribute("jsonoption", data.getJsonoption());
		ServletActionContext.getRequest().getSession().setAttribute("jsondata", data.getJsondata());
		return "BarLine";
	}
	
	/**
	 * 通用的下载模板
	 * @return
	 * @throws FileNotFoundException
	 */
	public String downMap() throws FileNotFoundException{
		File file=new File("E:/download/map.xls");
		filename=file.getName();
		inputStream=new FileInputStream(file);
		return "downMap";
	}
	
	/**
	 * Level的下载模板
	 * @return
	 * @throws FileNotFoundException
	 */
	public String downLevel() throws FileNotFoundException{
		File file=new File("E:/download/level.xls");
		filename=file.getName();
		inputStream=new FileInputStream(file);
		return "downLevel";
	}
	
	/**
	 * Line的下载模板
	 * @return
	 * @throws FileNotFoundException
	 */
	public String downLine() throws FileNotFoundException{
		File file=new File("E:/download/line.xls");
		filename=file.getName();
		inputStream=new FileInputStream(file);
		return "downLevel";
	}
}
