package hjy.data.vo;

import java.io.File;

public class Data {
	
	private String jsondata;
	
	private File excel; 
	private String excelContentType; 
	private String excelFileName;
	
	private String jsonoption;
	public String getJsonoption() {
		return jsonoption;
	}

	public void setJsonoption(String jsonoption) {
		this.jsonoption = jsonoption;
	}

	public String getJsondata() {
		return jsondata;
	}

	public void setJsondata(String jsondata) {
		this.jsondata = jsondata;
	}

	public File getExcel() {
		return excel;
	}

	public void setExcel(File excel) {
		this.excel = excel;
	}

	public String getExcelContentType() {
		return excelContentType;
	}

	public void setExcelContentType(String excelContentType) {
		this.excelContentType = excelContentType;
	}

	public String getExcelFileName() {
		return excelFileName;
	}

	public void setExcelFileName(String excelFileName) {
		this.excelFileName = excelFileName;
	}


	
	
}
