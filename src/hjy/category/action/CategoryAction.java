package hjy.category.action;

import java.util.List;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import hjy.category.service.CategoryService;
import hjy.category.vo.Category;
import hjy.user.vo.User;

public class CategoryAction extends ActionSupport implements ModelDriven<Category> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Category category=new Category();
	public Category getModel(){
		return category;
	}
	
	private CategoryService categoryService;
	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}
	
	//后台执行查询的方法
	public String findAll(){
		//查询所有
		List<Category> clist= categoryService.findAll();
		//将数据 显示到页面,把这个查询到的集合放到valuestack中去
		ActionContext.getContext().getValueStack().set("clist", clist);
		return "findAll";
	}
	
	//增加方法
	public String save(){
		//用模型驱动接受
		categoryService.save(category);
		return "saveSuccess";
	}
	
	//删除的方法
	public String delete(){
		categoryService.delete(category);
		return "delete";
	}
	
	//编辑的方法------先查询
	public String edit(){
		//根据分类id查询分类
		category=categoryService.findByCaid(category.getCaid());		
		//页面跳转
		return "editSuccess";
	}
	
	//编辑的方法------查询之后再修改
	public String update(){
		//修改
		categoryService.update(category);		
		return "updateSuccess";						
	}
	
	private String jsondata;
	
	public String getJsondata() {
		return jsondata;
	}

	public void setJsondata(String jsondata) {
		this.jsondata = jsondata;
	}
	
	private String result;
	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	
	
	public String test(){
		
		System.out.println(jsondata);
		return "test";
	}
	
	
	
	
}
