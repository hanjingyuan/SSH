package hjy.category.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import hjy.category.vo.Category;

public class CategoryDao extends HibernateDaoSupport{
	/**
	 * 1.dao层要注入sessionfactory
	 */
	
	//hjy.category.vo.Category
	public List<Category> findAll() {
		String hql="from Category";
		List<Category> list=this.getHibernateTemplate().find(hql);
		return list;
	}
	
	//保存方法
	public void save(Category category) {
		
		this.getHibernateTemplate().save(category);
		
	}
	
	//根据id查询
	public Category fingByCaid(int caid) {
		return this.getHibernateTemplate().get(Category.class, caid);
	}
	
	//删除
	public void delete(Category category) {
		this.getHibernateTemplate().delete(category);
	}
	
	//dao更新的方法
	public void update(Category category) {
		this.getHibernateTemplate().update(category);	
	}



}
