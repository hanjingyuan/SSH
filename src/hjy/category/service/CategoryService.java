package hjy.category.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import hjy.category.dao.CategoryDao;
import hjy.category.vo.Category;

/**
 * 
 * @author lucah
 *
 */
@Transactional
public class CategoryService {
	//注入dao
	private CategoryDao categoryDao;
	public void setCategoryDao(CategoryDao categoryDao) {
		this.categoryDao = categoryDao;
	}
	
	
	public List<Category> findAll() {
		
		return categoryDao.findAll();
	}


	public void save(Category category) {
		categoryDao.save(category);
	}
	
	//根据caid查询
	public Category findByCaid(int caid){
		
		return categoryDao.fingByCaid(caid);
	}


	public void delete(Category category) {
		categoryDao.delete(category);		
	}

	
	//修改编辑的方法
	public void update(Category category) {
		
		categoryDao.update(category);
		
	}

}
